/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef SHOPPER_ITEM_H
#define SHOPPER_ITEM_H

#include <QObject>

#include <iostream>
#include <list>
#include <set>
#include <map>
using namespace std;

namespace Shopper
{
	// some forward declarations...
	class List;
	class Category;
	class Item;

	// normal functions
	bool operator< (const Item &a, const Item &b);
	bool cmpItem(const Item *a, const Item *b);

	class Item : public QObject
	{
		Q_OBJECT;

	private:
		typedef enum sparseItem_ {SPARSE} sparseItem; // Only friends know about 
		Item(sparseItem dummy);   // This constructor is for friends to instantiate objects

	public:
		Item();
		Item(Category &c, QString desc, QString note, bool wanted, bool bought);
		~Item();

		friend bool operator< (const Item &a, const Item &b);
		friend bool cmpItem(const Item *a, const Item *b);
		void dbg();
		
		// accessors
		QString print_id();
		int get_id() const;
		void set_id(int);
		void set_desc(QString);
		QString get_desc() const;
		void set_note(QString);
		QString get_note() const;
		void set_wanted(bool);
		bool get_wanted() const;
		void set_bought(bool);
		bool get_bought() const;
		void set_category(Category *c);
		Category* get_category();
	
	private:
		int            id;           /* The id of the item (why?) */
		static int     id_master;    /* Used for uniqueness */
		Category      *category;     /* The id of the category the item is in */
		QString  desc;         /* The item desc */
		QString  note;         /* User note (flavour, quantity)*/
		bool       wanted;       /* do we want one of these? */
		bool       bought;       /* when out shopping, have we bought one of these */

	signals:
		void deleted();
		void changed();


		friend class ListParser;
		friend class XMLWriter;
	};
}

#endif // SHOPPER_ITEM_H
