/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1
#include "LabelEntry.h"
#include "ActiveLabel.h"
#include "shopper.h"           // automake, i8n, gettext
#include "GestureWatcher.h"
#include <QWidget>
#include <QLabel>
#include <QString>
#include <QMouseEvent>
#include <QLineEdit>
#include <QSizePolicy>
#include <QFont>

namespace Shopper
{
	// LabelEntry - a label that turns into a textbox when clicked
	LabelEntry::LabelEntry(QWidget *parent) :
		QWidget(parent),
		data(""),
		entry(0),
		layout(this)
	{
		init();
	}
	LabelEntry::LabelEntry(const QString & value, QWidget *parent) :
		QWidget(parent),
		data(value),
		entry(0),
		layout(this)
	{
		init();
	}
	void LabelEntry::init()
	{
		myLabel = new ActiveLabel(data, this);
		GestureWatcher* gw = GestureWatcher::getGestureWatcher();
		gw->connect(myLabel, Gesture("l "),
				   this, SLOT(label_clicked()));
		gw->connect(myLabel, Gesture("r "),
				   this, SLOT(label_clicked()));
//		connect(myLabel, SIGNAL(pressed()),
//				this, SLOT(label_clicked()));
		layout.addWidget(myLabel);
		layout.setContentsMargins(0, 0, 0, 0);
		myLabel->show();
	}
	QString LabelEntry::getText()
	{
		return data;
	}

	void LabelEntry::label_clicked()
	{
		_ENTER;
		if (entry != 0) return; // events may arrive post-creation
		entry = new QLineEdit(this);
		entry->setSizePolicy(myLabel->sizePolicy());
//		entry->set_activates_default(false);
//		entry->set_width_chars(15); // FIXME
		entry->setText(data);
		// self-destruct on unfocus or activate
		connect(entry, SIGNAL(editingFinished()),
				this, SLOT(entry_finished()));
	
		layout.removeWidget(myLabel);
		myLabel->hide();
		layout.addWidget(entry);
		entry->show();
		entry->setFocus(Qt::MouseFocusReason);
	}
	void LabelEntry::entry_finished()
	{
		if (entry == 0) return; // events may arrive post-deletion
		data=entry->text();
		myLabel->setText(data);
		layout.removeWidget(entry);
		entry->hide();
		layout.addWidget(myLabel);
		myLabel->show();
		entry->deleteLater();   // We can now forget about it...
		entry=0;
		emit changed();
	}
	void LabelEntry::setAlignment ( Qt::Alignment al )
	{
		myLabel->setAlignment(al);
	}
	void LabelEntry::setText ( const QString & d )
	{
		data = d;
	}
	void LabelEntry::setVisible(bool vis)
	{
		QWidget::setVisible(vis);
	}

	void LabelEntry::setFrameStyle(int i)
	{
		myLabel->setFrameStyle(i);
	}
	void LabelEntry::setSizePolicy(QSizePolicy::Policy h, QSizePolicy::Policy v)
	{
		QWidget::setSizePolicy(h,v);
		myLabel->setSizePolicy(h,v);
	}
	void LabelEntry::setSizePolicy(QSizePolicy p)
	{
		QWidget::setSizePolicy(p);
		myLabel->setSizePolicy(p);
	}
	void LabelEntry::setFont ( const QFont & f)
	{
		myLabel->setFont(f);
		if (entry) entry->setFont(f);
	}
	QLabel* LabelEntry::label () { return myLabel; }
}
