/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

//#define DEBUG_SHOPPER 1
#include "Gesture.h"
#include "shopper.h"           // automake, i8n, gettext

#include <QList>
#include <QHash>

GestureConnectionStore::GestureConnectionStore() : QObject() {};

struct Gesture::Private
{
	// available via public API
	QString  shape;     // holds the string encoding of the shape.

	// avialable via friend API, populated by 'connect'
	GestureConnectionStore * connectionStore;
};


// Public
Gesture::Gesture(QString shape)
{
	p = new Private;
	p->shape = shape;
	p->connectionStore = 0;
}
// Protected
Gesture::Gesture(Gesture *g)
{
	p = new Private;
	p->shape = g->p->shape;
	p->connectionStore = 0;
}

Gesture::Gesture(const Gesture &g)
{
	p = new Private;
	p->shape = g.p->shape;
	p->connectionStore = 0;
}

Gesture::~Gesture()
{
	if (p->connectionStore)
		delete p->connectionStore;

	delete p;
}

////////////////////////////////////////////////////////////////
// operators
bool operator==(const Gesture &A, const Gesture &B)
{
	return A.p->shape == B.p->shape;
}

uint qHash(const Gesture &A)
{
	QString s=A.p->shape;
	return qHash(s);
}


////////////////////////////////////////////////////////////////
// Public interface
void Gesture::addStroke(QString stroke)
{
	p->shape+=stroke;
}

QString Gesture::shape() const
{
	return p->shape;
}


////////////////////////////////////////////////////////////////
// Protected interface
void Gesture::connect(QObject* receiver, const char* method)
{
	if 	(p->connectionStore == 0)
		p->connectionStore = new GestureConnectionStore;

	p->connectionStore->connect(p->connectionStore, SIGNAL(activated()),
								receiver, method);
}

bool Gesture::disconnect(QObject* receiver, const char* method)
{
	Q_ASSERT(p->connectionStore != 0);
	if (p->connectionStore == 0)
		return false;

	return p->connectionStore->disconnect(p->connectionStore, SIGNAL(activated()),
										  receiver, method);
}

void Gesture::invoke()
{
	if 	(p->connectionStore)	
		p->connectionStore->activated();
}
