/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1
#include "ItemDialog.h"
#include "shopper.h"           // automake, i8n, gettext

#include "CategoryDialog.h"
#include "GestureWatcher.h"

#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QHBoxLayout>

namespace Shopper
{
////////////////////////////////////////////////////////////////
// Public
	ItemEdit::ItemEdit(QWidget * parent, Shopper::List* l, Shopper::Item* it) :
		ItemDialog(parent, l, it)
	{}
	ItemAdd::ItemAdd(QWidget * parent, Shopper::List* l) :
		ItemDialog(parent, l, 0)
	{}

////////////////////////////////////////////////////////////////
// Private
	ItemDialog::ItemDialog( QWidget * parent, Shopper::List* list, Shopper::Item* it) :
		QDialog(parent),
		mylist(list),
		myitem(it)
	{
		_ENTER;
//		GestureWatcher* gw = GestureWatcher::getGestureWatcher();
//		gw->connect(this, Gesture("dr ur "), this, SLOT(accept()));

		// Item details
		descLabel = new QLabel(tr("Item:"),this);
		desc_e = new QLineEdit(this);
		// Category widgets
		catLabel = new QLabel(tr("Category:"),this);
		
		catCombo = new QComboBox(this);
		model = new CatListModel(*mylist, false);
		catCombo->setModel(model);
		catCombo->setModelColumn(1);
		
		plusButton = new QPushButton(QIcon("/usr/share/icons/hicolor/64x64/apps/shopper/categories.png"),
									 "More>>", this);  // FIX: Should be the + icon
		connect(plusButton, SIGNAL(clicked()),
				this, SLOT(plus_category()));

		// Buttons depending on whether we're adding an item or editing it
		QDialogButtonBox* buttonBox = new QDialogButtonBox(Qt::Horizontal, this);
		buttonBox->addButton(QDialogButtonBox::Cancel);
		if (myitem) {
			catCombo->setCurrentIndex(model->indexOfCat(myitem->get_category()));
			buttonBox->addButton(tr("OK"), QDialogButtonBox::AcceptRole);
			setWindowTitle("Edit Item");
			desc_e->setText(myitem->get_desc());
		} else {
			catCombo->setCurrentIndex(model->currentIndex()); // Use current category for new items
			buttonBox->addButton(tr("Add"), QDialogButtonBox::AcceptRole);
			setWindowTitle("Add Item");
		}
		connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
		connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

		// Now handle the data if we're accepted
		connect(this, SIGNAL(accepted()),
				this, SLOT(add_or_edit()));
		
		// Lay it all out
		QHBoxLayout *topLayout = new QHBoxLayout();
		topLayout->addWidget(descLabel);
		topLayout->addWidget(desc_e);
		QHBoxLayout *midLayout = new QHBoxLayout();
		midLayout->addWidget(catLabel);
		midLayout->addWidget(catCombo);
		midLayout->addWidget(plusButton);
		QVBoxLayout *mainLayout = new QVBoxLayout(this);
		mainLayout->addLayout(topLayout);
		mainLayout->addLayout(midLayout);
		mainLayout->addWidget(buttonBox);

		setLayout(mainLayout);
	}
	ItemDialog::~ItemDialog()
	{
		delete model;
	}
	
	void ItemDialog::plus_category()
	{
		Shopper::CategoryManage catD(this, mylist);
		catD.exec();
	}

	void ItemDialog::add_or_edit()
	{
		_ENTER;
		if (myitem) {
			DEBUG("EDIT\n");
			myitem->set_desc(desc_e->text());
			myitem->set_category(get_category());
			DEBUG("\nEDITED:\n");
			myitem->dbg();
		} else {
//			DEBUG("ADD\n");
			myitem = new Shopper::Item(*get_category(),
									   desc_e->text(), "", true, false);
//			DEBUG("\nADDED:\n");
			myitem->dbg();
			mylist->add(*myitem);
		}

	}

	Category* ItemDialog::get_category()
	{
		QVariant v = catCombo->itemData(catCombo->currentIndex());
//		DEBUG("ComboBox set to category " << v.value<Category*>()->get_name());
		return(v.value<Category*>());
	}
}
