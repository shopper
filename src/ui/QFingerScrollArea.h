/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef SHOPPER_UI_QFINGERSCROLLAREA_H
#define SHOPPER_UI_QFINGERSCROLLAREA_H

#include <QScrollArea>
#include <QTime>
#include <QBasicTimer>

class QFingerScrollArea: public QScrollArea
	{
		Q_OBJECT;
		
	public:
		QFingerScrollArea(QWidget * parent = 0);

		// New interface
		void setScrollConstraints(bool AllowHoriz, bool AllowVert);
		void setKinetic(bool kinetic);              // Continue moving after release
//		bool kinetic();
		void setScrollPaper(bool scrollPaper);      // true then simulate finger dragging the widget
//		bool scrollPaper();                         // false then scroll like a scrollbar

		
		// Operational
		bool eventFilter(QObject *obj, QEvent *event);

		// constants
		static const int SENSITIVITY, KINETIC_REFRESH, VSCALE,
			DECEL_DURATION, DEBOUNCE;
		static const double DECEL;
	public:
		void registerChildrenForFingerScrolling(QObject *top);
		void deregisterChildrenForFingerScrolling(QObject *top);
		void mousePressEvent ( QMouseEvent * event );
		void mouseMoveEvent ( QMouseEvent * event );
		void mouseReleaseEvent ( QMouseEvent * event );
		void childEvent ( QChildEvent * event );
		
	private:
		bool allowHoriz, allowVert; // Should we scroll horiz and/or vert
		bool scrollPaper;
		int direction;               // +1/-1
		bool scrolling;              // State: are we scrolling?
		bool recently_scrolling;     // were we scrolling?
		bool scrolling_setup;        // are we setup for scrolling?
		int scroll_start_x, scroll_start_y;    // scrollbar value at fingerdown to establish base position
		int start_x, start_y;        // global event x,y at start
		int curr_x, curr_y;          // current x,y (kept to allow velocity calc)

		int max_x, max_y;            // screen size for scaling
		int scroll_range_x;          // scroll range horiz
		int scroll_range_y;          // scroll range vert
		int scale_x, scale_y;        // scale (ie scroll by pixels or by scrollbar)
		bool kinetic;                // Should we be a little kinetic
		QTime event_time;
		int last_x, last_y;          // global event x,y at start
		int vel_x, vel_y;            // Velocity
		int vel_x1, vel_y1;          // Velocity before
		int last_ev_time, curr_time; // timer for velocity
		
		int kineticTimer;
		int debounceTimer;
		void timerEvent(QTimerEvent *event);
		int kinetic_cycles;          // how long do we slow down for

		void setupEvent ( QMouseEvent * event );

//		bool event ( QEvent * e); 
	};

#endif // SHOPPER_UI_QFINGERSCROLLAREA_H
