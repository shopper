/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1
#include "NoteWidget.h"
#include "shopper.h"           // automake, i8n, gettext
#include "GestureWatcher.h"
#include <QWidget>
#include <QLabel>
#include <QString>
#include <QMouseEvent>
#include <QLineEdit>
#include <QSizePolicy>
#include <QFont>

namespace Shopper
{
	// NoteWidget - Shopper note widget
	NoteWidget::NoteWidget(QWidget *parent) :
		QLabel(parent),
		data(""),
		entry(0)
	{
		init();
	}
	NoteWidget::NoteWidget(const QString & value, QWidget *parent) :
		QLabel(parent),
		data(value),
		entry(0)
	{
		init();
	}
	void NoteWidget::init()
	{
		GestureWatcher* gw = GestureWatcher::getGestureWatcher();
		gw->connect(this, Gesture("l "),
				   this, SLOT(labelClicked()));
		gw->connect(this, Gesture("r "),
				   this, SLOT(labelClicked()));
	}

	QString NoteWidget::text()
	{
		return data;
	}
	void NoteWidget::setText ( const QString & d )
	{
		data = d;
		QLabel::setText(data.isEmpty() ? "" : "("+data+")");  // display () if there is a note.
	}
	void NoteWidget::labelClicked()
	{
		_ENTER;
		if (entry != 0) return; // events may arrive post-creation
		QLabel::setText("");
		entry = new QLineEdit(this);
		entry->setSizePolicy(sizePolicy());
		entry->setText(data);
		entry->setFont(QLabel::font());
		// self-destruct on unfocus or activate
		connect(entry, SIGNAL(editingFinished()),
				this, SLOT(entryFinished()));
		entry->show();
		entry->setFocus(Qt::MouseFocusReason);
	}
	void NoteWidget::entryFinished()
	{
		if (entry == 0) return; // events may arrive post-deletion
		data=entry->text();
		QLabel::setText(data.isEmpty() ? "" : "("+data+")");
		entry->hide();
		entry->deleteLater();   // We can now forget about it...
		entry=0;
		emit changed();
	}
	void NoteWidget::setFont ( const QFont & f)
	{
		QLabel::setFont(f);
		if (entry) entry->setFont(f);
	}
}
