/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef NOTEWIDGET_H
#define NOTEWIDGET_H

#include "shopper.h"           // automake, i8n, gettext
#include <QString>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QMouseEvent>

namespace Shopper
{
// NoteWidget - represent a shopper Note
	class NoteWidget : public QLabel
	{
		Q_OBJECT;			
	public:
		NoteWidget(QWidget *parent = 0);
		explicit NoteWidget(const QString & note, QWidget *parent = 0);
	private:
		void init();  // real constructor
	public:
		QString text();
		void setText( const QString & d);
		void setFont( const QFont & f);
		
	protected:
		QString       data;
		QLineEdit     *entry;

	public slots:
		void labelClicked();
		void entryFinished();
	signals:
		void changed();
	};
}
#endif // NOTEWIDGET_H
