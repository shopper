/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

//#define DEBUG_SHOPPER 1
#include "ItemView.h"
#include "shopper.h"           // automake, i8n, gettext
#include "ItemDialog.h"
#include "LabelEntry.h"
#include "NoteWidget.h"
#include <QSettings>
#include <QPalette>
#include <QColor>
#include <QSizePolicy>
#include <QCheckBox>
#include <QFrame>
#include <QContextMenuEvent>
#include <QMenu>
#include <QFont>
#include <iostream>
#include "GestureWatcher.h"

using namespace std;

namespace Shopper
{
	const int ItemView::MINFONTSIZE = 14;
	const int ItemView::MAXFONTSIZE = 28;
	ItemView::ItemView(Item &it, List &l, QWidget *parent) :
		QWidget(parent),
		outer_b(),
		inner_b(),
		note(0),
		myitem(&it),
		mylist(&l)
	{
		QSettings settings;
		bool showTwoColumns = settings.value("ui/ShowTwoColumns").toBool();
//		setStyleSheet("color : green;margin: 0px;border: 0px ;padding: 0px ;");
//		setMargin(0);
		QPalette p = palette();
		p.setColor(QPalette::WindowText, QColor(0,0,0));
		setPalette(p);
		
		QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
		sizePolicy.setHorizontalStretch(0);
		sizePolicy.setVerticalStretch(0);
		setSizePolicy(sizePolicy);

		bool active = (mylist->get_state() == MAKING_LIST) ? myitem->get_wanted() : myitem->get_bought();
		tick = new QCheckBox(this);
		// The tick represents either wanted or bought
		tick->setChecked(active);
		desc = new ActiveLabel(myitem->get_desc(), this);
		desc->setAlignment(Qt::AlignLeft);
		desc->setSizePolicy(sizePolicy);
		desc->setMargin(0);
		QFont font = desc->font();
		if (mylist->get_state() == MAKING_LIST)
			font.setStrikeOut(false);
		else
			font.setStrikeOut(active);
		desc->setFont(font);
				
		// Lay them out
		outer_b.addWidget(tick);
		outer_b.addLayout(&inner_b,1);
		inner_b.addWidget(desc, 0);

		// maybe show a note
		if (! showTwoColumns) {
			note = new NoteWidget(myitem->get_note(),this);
			note->setAlignment(Qt::AlignLeft);
			note->setSizePolicy(sizePolicy);
			p.setColor(QPalette::WindowText, QColor(150,150,150));
			note->setPalette(p);
//			note->setStyleSheet("border: 1px solid gray");
//			note->label()->setMargin(0);
//			note->label()->setLineWidth(2);
//			note->label()->setMidLineWidth(1);
//			note->label()->setFrameStyle(QFrame::Panel | QFrame::Sunken);

			inner_b.addWidget(note, 1);
			// Respond to clicking the note
			connect(note, SIGNAL(changed()),
					 this, SLOT(note_changed()));
		 }

		 outer_b.setContentsMargins(0, 0, 0, 0);
		 inner_b.setContentsMargins(0, 0, 0, 0);
		 // and apply
		 setLayout(&outer_b);

		 // Respond to user clicking the checkbox.
		 connect(tick, SIGNAL(stateChanged(int)),
				 this, SLOT(tick_changed(int)));

		 // We gesture over the description to activate it
		 GestureWatcher* gw = GestureWatcher::getGestureWatcher();
		 gw->connect(desc, Gesture("l "),
					this, SLOT(desc_clicked()));
		 gw->connect(desc, Gesture("r "),
					this, SLOT(desc_clicked()));

		 // And clicking the description is the same.
 //		connect(desc, SIGNAL(pressed()),
 //				this, SLOT(desc_clicked()));

		 // Now connect the item's changed signal to this object's updater
		 connect(&it, SIGNAL(changed()),
				 this, SLOT(updateVisibility()));

		 show();
		 DEBUG("ItemView created for " <<myitem->get_desc());
	 }

	 // Prepare menu actions at time of popup
	 void ItemView::contextMenuEvent( QContextMenuEvent * event )
	 {
		 _ENTER;
		 QMenu menu(this);
		 connect(menu.addAction(tr("Edit")),
				 SIGNAL(triggered()), this, SLOT(edit_item()));
		 connect(menu.addAction(tr("Delete")),
				 SIGNAL(triggered()), this, SLOT(delete_item()),
				 Qt::QueuedConnection);
		 menu.exec(event->globalPos());
		 event->accept();
	 }

 // Only shows if matching item should be seen.
	 void ItemView::setVisible(bool vis)
	 {
		 // Hide if not wanted
		 if (((mylist->get_state() == WHATS_LEFT) || (mylist->get_state() == OUT_SHOPPING))
			 && ! myitem->get_wanted()) {
			 QWidget::setVisible(false);
			 return;
		 }		

		 if ((mylist->get_state() == WHATS_LEFT) && myitem->get_bought()) {
			 QWidget::setVisible(false);
			 return;
		 }
		 QWidget::setVisible(vis);
		 return;
	 }

	 void ItemView::note_changed()
	 {
		 myitem->set_note(note->text()); // Setting the data will call
										 // an update on the note
	 }
	 void ItemView::edit_item()
	 {
		 ItemEdit itemD(this, mylist, myitem);
		 itemD.exec();
	 }
	 void ItemView::delete_item()
	 {
		 DEBUG("Delete Item\n");
		 mylist->rm(*(this->myitem));
	 }
	 void ItemView::desc_clicked()
	 {
		 // Make the tick tock
		 DEBUG("Desc pressed\n");
		 tick->toggle(); // Invokes tick_changed
		 tick->setFocus(Qt::MouseFocusReason);
	 }
	 void ItemView::tick_changed(int s)
	 {
		 bool active = (s == Qt::Checked);
		 // Suspend our data watch - we're making the change and don't need
		 // to be notified
		 DEBUG("Tick " << active <<"\n");
		 switch (mylist->get_state()) {
		 case OUT_SHOPPING:
			 DEBUG("shopping ");
		 case WHATS_LEFT:
			 DEBUG("what's left\n");
			 myitem->set_bought(active);
			 break;
		 case MAKING_LIST:
			 DEBUG("Making list\n");
			 myitem->set_wanted(active);
			 break;
		 default:
			 DEBUG("State invalid\n");
		 }
	 }

 // When the underlying data changes, 
	 void ItemView::updateVisibility()
	 {
		 DEBUG("Notified of item change: updating view\n");
		 bool active = (mylist->get_state() == MAKING_LIST) ? myitem->get_wanted() : myitem->get_bought();

		 tick->setChecked(active);
		 QFont font = desc->font();
		 if (mylist->get_state() == MAKING_LIST)
			 font.setStrikeOut(false);
		 else
			 font.setStrikeOut(active);
		 desc->setFont(font);

		 desc->setText(myitem->get_desc());
		 if (note)  // may not have a note in 2-column mode
			 note->setText(myitem->get_note());

		 if (! mylist->is_category_active(*(myitem->get_category())))
			 hide();
		 else
			 show();
	 }

#define BASEFONTSIZE 14
#define MINFONTSIZE 14
#define MAXFONTSIZE 28
	void ItemView::setZoom(int z)
	{
		QFont font = desc->font();
		int s = BASEFONTSIZE + 2*z;
 		if (s < MINFONTSIZE) s=MINFONTSIZE;
 		if (s > MAXFONTSIZE) s=MAXFONTSIZE;
		font.setPointSizeF(s);
		desc->setFont(font);
		if (note) note->setFont(font);
	}
}
