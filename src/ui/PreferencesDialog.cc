/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1
#include "PreferencesDialog.h"
#include "shopper.h"           // automake, i8n, gettext

#include <QWidget>
#include <QGridLayout>
#include <QTabWidget>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QSettings>
#include <QLabel>
#include <QCheckBox>

namespace Shopper
{
////////////////////////////////////////////////////////////////
// Public
	PreferencesDialog::PreferencesDialog( QWidget * parent) :
		QDialog(parent),
		signalMapper(this)
	{
		_ENTER;

		// UI pane and options
		uiPane = new QWidget;
		QGridLayout *uiLayout = new QGridLayout(uiPane);
		// Note that addPref returns a QCheckBox* chk that may be
		// useful for a connect() to action a preference change in the UI.
		addPref("ui/ShowCategoryHeadings", true, tr("Show Category Headings"), uiLayout, 0);
		addPref("ui/RotateOtherWay", false, tr("Rotate the other way"), uiLayout, 1);
		addPref("ui/RotateSafe", false, tr("Rotate to 'normal' on Exit"), uiLayout, 2);
//		addPref("ui/ShowTwoColumns", false, tr("Show Two Columns"), uiLayout, 2);

		// Data pane and options
		dataPane = new QWidget;
		QGridLayout *dataLayout = new QGridLayout(dataPane);
		addPref("data/SaveOnExit", true, tr("Save On Exit"), dataLayout, 0);

		// Mapped settings send the "setting" for every toggle
		connect(&signalMapper, SIGNAL(mapped(const QString &)),
				this, SLOT(toggleSetting(const QString &)));
		
		// Add the panes to the tab widget
		tabWidget = new QTabWidget;
		tabWidget->addTab(uiPane, tr("View"));
		tabWidget->addTab(dataPane, tr("Data"));

		buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
		connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));

		QVBoxLayout *mainLayout = new QVBoxLayout;
		mainLayout->addWidget(tabWidget);
		mainLayout->addWidget(buttonBox);
		setLayout(mainLayout);
	
	}

////////////////////////////////////////////////////////////////
// Private
	QCheckBox* PreferencesDialog::addPref(const QString setting, bool defaultVal, QString label, QGridLayout *layout, int row)
	{
		QSettings settings;
		bool value = settings.value(setting, defaultVal).toBool();
		settings.setValue(setting, value); // ensure that any default is saved
		QLabel    *L     = new QLabel(label);
		QCheckBox *Check = new QCheckBox();
		layout->addWidget(L, row, 0);
		layout->addWidget(Check, row, 1);
		Check->setChecked(value);
		signalMapper.setMapping(Check, setting);
		connect(Check, SIGNAL(released()),     // ugh - but signalMapper can't cope with anything other than signal()
				&signalMapper, SLOT(map()));
		return Check;
	}

	void PreferencesDialog::toggleSetting(const QString setting)
	{
		_ENTER;
		QSettings settings;
		bool value = settings.value(setting).toBool();
		settings.setValue(setting, ! value); // invert
		DEBUG("Set " << setting << " from " << value << " to " << settings.value(setting).toBool());
	}
}
