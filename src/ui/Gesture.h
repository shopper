/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef GESTURE_H
#define GESTURE_H
#include <QObject>

class GestureConnectionStore : public QObject
{
	Q_OBJECT;
	GestureConnectionStore();
signals:
	void activated();
	friend class Gesture;
};

// A Gesture is a shape
class Gesture
{
public:
	Gesture(QString shape);
	Gesture(const Gesture &g);               // Copy constructor
    ~Gesture();

	void addStroke(QString stroke);
	QString shape() const;

protected:
	friend class GestureWatcher;      // Can see these methods
	friend bool operator==(const Gesture &A, const Gesture &B);
	friend uint qHash(const Gesture &A);

	Gesture(Gesture *g);              // Copy constructor
	void connect(QObject* receiver, const char* method);
	bool disconnect(QObject* receiver, const char* method);
	void invoke();
	
private:
	struct Private;
	struct Connection;
	Private *p;

};

bool operator==(const Gesture &A, const Gesture &B);
uint qHash(const Gesture &A);
	
#endif //GESTURE_H
