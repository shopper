/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef SHOPPER_UI_ITEMVIEW_H
#define SHOPPER_UI_ITEMVIEW_H

#include <QWidget>
#include <QHBoxLayout>

class QCheckBox;
class QContextMenuEvent;

namespace Shopper
{
	class Item;
	class List;
	class ActiveLabel;
	class NoteWidget;
	class ItemView: public QWidget
	{
		Q_OBJECT;
		
	public:
		ItemView(Shopper::Item &it, Shopper::List &l, QWidget *parent);
	
		virtual void setVisible(bool vis);
		QCheckBox *tick;       /* display either the 'bought/not
									  * bought' or the 'wanted/not wanted'
									  * state depending on the current
									  * mode.  This also includes the
									  * label
									  */
		QHBoxLayout      outer_b;
		QHBoxLayout      inner_b;
		ActiveLabel      *desc;       /* This displays the item description */
		NoteWidget       *note;       /* This displays the notes. When
									   * clicked on it transforms into a
									   * textEntry */
		Item      *myitem;     /* the item being displayed */
		List      *mylist;     /* the parent list */
		
		// This handles right clicks (or equiv)
		void contextMenuEvent( QContextMenuEvent * event );
		static const int BASEFONTSIZE, MINFONTSIZE, MAXFONTSIZE;

	public slots:
		// signal handlers
		void desc_clicked();     // toggles the check
		void tick_changed(int s);// changes the state when check changes
		void note_changed();     // EntryLabel calls to change note 
		void edit_item();        // menu calls this to open edit dialog
		void delete_item();      // menu calls this to removes item
		void setZoom(int);
	
		// data monitors
		void updateVisibility();
	};
}

#endif // SHOPPER_UI_ITEMVIEW_H
