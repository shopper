/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef SHOPPER_UI_CATEGORYDIALOG_H
#define SHOPPER_UI_CATEGORYDIALOG_H

#include "shopperList.h"
#include <QMetaType>
#include <QWidget>
#include <QDialog>
#include "LabelEntry.h"
#include "QFingerScrollArea.h"

namespace Shopper
{
	// private helper class
	class CatView : public QWidget
	{
		Q_OBJECT;
	public:
		CatView(Category &it, List &l,QWidget * parent = 0) ;
		~CatView() ;

		Category* get_cat();
		void set_pos(int, int);
//		virtual void setVisible(bool vis);

	public slots:
		void name_changed();
		void delete_category();
		void activate();
		
	signals:
		void move_up();
		void move_down();

	private:
		struct Private;
		Private *p;
	};
}
// Allow CatView* to be passed as Qt::QueuedConnection params
// must be done outside the namespace
class QVBoxLayout;
class QDialogButtonBox;
class QScrollArea;
Q_DECLARE_METATYPE ( Shopper::CatView * )
namespace Shopper
{
		
// private base for adding/editing an Category
	class CategoryDialog : public QDialog
	{
		Q_OBJECT;
	protected:
		CategoryDialog(QWidget * parent, Shopper::List*, Shopper::Category*);
		~CategoryDialog();
		
	public slots:
		void update_area();
		void add_category();
		void move_up();
		void move_down();
		void highlightCategory(CatView *);

	signals:
		void categoryAdded(CatView *);

	private:
		void move(Shopper::CatView *c, int direction);

		struct Private;
		Private *p;

	};

// Public classes for managing
class CategoryManage : public CategoryDialog
	{
	public:
		CategoryManage(QWidget * parent, Shopper::List* mylist);
	};
}
#endif
