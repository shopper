/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef LABELENTRY_H
#define LABELENTRY_H

#include "shopper.h"           // automake, i8n, gettext
#include "ActiveLabel.h"
#include <QString>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QMouseEvent>

namespace Shopper
{
// LabelEntry - a label that turns into a textbox when clicked
	class LabelEntry : public QWidget
	{
		Q_OBJECT;			
	public:
		LabelEntry(QWidget *parent = 0);
		explicit LabelEntry(const QString & label, QWidget *parent = 0);
	private:
		void init();  // real constructor
	public:
		QString getText();
		void setAlignment ( Qt::Alignment al );
		void setText ( const QString & d);
		virtual void setVisible(bool vis);
		void setFrameStyle(int i);
		void setSizePolicy(QSizePolicy::Policy h, QSizePolicy::Policy v);
		void setSizePolicy(QSizePolicy p);
		void setFont ( const QFont & );
		QLabel* label();
		
	protected:
		QString       data;
		ActiveLabel   *myLabel;
		QLineEdit     *entry;
		QHBoxLayout   layout;

	public slots:
		void label_clicked();
		void entry_finished();
	signals:
		void changed();
	};
}
#endif // LABELENTRY_H
