/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef GESTUREWATCHER_H
#define GESTUREWATCHER_H

#include "shopper.h"           // automake, i8n, gettext
#include "Gesture.h"
#include "GestureReader.h"
#include <QWidget>
#include <QMouseEvent>

class GestureWatcher : public QObject
{
public:
	static GestureWatcher* getGestureWatcher();
	bool connect(QWidget *w, const Gesture &g,
				 QObject *receiver, const char * method);
	bool disconnect(QWidget *w, const Gesture &g = Gesture(""),
				 QObject *receiver = 0, const char * method = 0);
	//  w             disconnect all gestures from this widget
	//  w,g           disconnect all target/methods for this gestures on this widget
	//  w,g,r         disconnect all methods for this target for this gestures on this widget
	//  w,g,r,m       disconnect this methods for this target for this gestures on this widget

protected:
	GestureWatcher();
    ~GestureWatcher();

	static const int SENSITIVITY, MULTISTROKE, START;
	// Manage the heirarchy of widgets
	void registerHeirarchyForGestures(QObject *top);
	void deregisterHeirarchyForGestures(QObject *top);

	// manage incoming events
	bool eventFilter( QObject *obj, QEvent *event );
	void timerEvent(QTimerEvent *event);
	void replayEvents();

	// Helper
	void clearEvents();

	// Private data structure...
	struct Private;
	struct WidgetInfo;
	struct Watched;

	struct Private *p;
	// The singleton
	static GestureWatcher* theWatcher;
};



#endif //GESTUREWATCHER_H
