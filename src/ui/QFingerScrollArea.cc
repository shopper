/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1
#include "QFingerScrollArea.h"
#include "shopper.h"           // automake, i8n, gettext
#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QScrollBar>
#include <QMouseEvent>
#include <QTimerEvent>
#include <QChildEvent>
#include <QList>

const int QFingerScrollArea::SENSITIVITY     = 20;   // pixels before we scroll
//const double QFingerScrollArea::SCALE        = 0.1;  // sensitivity to movement
const int QFingerScrollArea::KINETIC_REFRESH = 100;  // ms
const int QFingerScrollArea::VSCALE          = 100;  // velocity scaling to ensure not lost in int rounding
const double QFingerScrollArea::DECEL        = 0.75; // velocity reduction factor/kinetic refresh
const int QFingerScrollArea::DECEL_DURATION  = 1000; // ms
const int QFingerScrollArea::DEBOUNCE        = 200;  // after a mouseRelease we disallow MousePress for this long

// QFingerScrollArea a QScrollArea that responds to fingers
QFingerScrollArea::QFingerScrollArea(QWidget * parent) :
	QScrollArea(parent),
	allowHoriz(false),
	allowVert(true),
	scrollPaper(true),
	direction(1),
	scrolling(false),
	recently_scrolling(false),
	scrolling_setup(false),
	scroll_start_x(0),
	scroll_start_y(0),
	start_x(0),
	start_y(0),
	curr_x(0),
	curr_y(0),
	scroll_range_x(0),
	scroll_range_y(0),
	scale_x(0),
	scale_y(0),
	kinetic(true),
	last_x(0),
	last_y(0),
	vel_x(0),
	vel_y(0),
	vel_x1(0),
	vel_y1(0),
	last_ev_time(0),
	curr_time(0),
	kineticTimer(0),
	debounceTimer(0),
	kinetic_cycles(0)
{
	max_x= qApp->desktop()->screenGeometry().width();
	max_y= qApp->desktop()->screenGeometry().height();
}

////////////////////////////////////////////////////////////////
// Interface
void QFingerScrollArea::setScrollConstraints(bool AllowHoriz, bool AllowVert)
{
	allowVert = AllowVert;
	allowHoriz = AllowHoriz;
	if (!(allowHoriz or allowVert)) allowVert = true; // Don't 
}
void QFingerScrollArea::setScrollPaper(bool _scrollPaper)
{
	scrollPaper = _scrollPaper;
}
void QFingerScrollArea::setKinetic(bool _kinetic)
{
	kinetic = _kinetic;
}
////////////////////////////////////////////////////////////////
// Finger scroll implementation - movement
void QFingerScrollArea::mousePressEvent ( QMouseEvent * event )
{
	if (event->button() == Qt::LeftButton){
		setupEvent ( event );
	}
	QScrollArea::mousePressEvent(event);
}
void QFingerScrollArea::setupEvent ( QMouseEvent * event )
{
	start_y = event->globalY();
	scroll_start_y = verticalScrollBar()->value();
	scroll_range_y = verticalScrollBar()->maximum() - verticalScrollBar()->minimum();
		
	start_x = event->globalX();
	scroll_start_x = horizontalScrollBar()->value();
	scroll_range_x = horizontalScrollBar()->maximum() - horizontalScrollBar()->minimum();

	if (scrollPaper) {
		scale_x = widget()->width()-width();
		scale_y = widget()->height()-height();
		direction = 1;
	} else {
		scale_x = width();
		scale_y = height();
		direction = -1;
	}
	event_time.start();
	last_ev_time=0;
	scrolling_setup = true;
	DEBUG("scroll setup " <<start_x<<":"<<start_y);
}
void QFingerScrollArea::mouseMoveEvent ( QMouseEvent * event )
{
	// We should get a mousePressedEvent - but we don't always...
	if (!scrolling_setup) {
		setupEvent(event);
	}

	if (abs(start_x - event->globalX()) > SENSITIVITY or
		abs(start_y - event->globalY()) > SENSITIVITY) {
		if ((allowVert and scroll_range_y) or
			(allowHoriz and scroll_range_x))
			scrolling = true;
	}
	if (!scrolling) {
		QScrollArea::mouseMoveEvent(event);
		return;
	}
	curr_y = event->globalY();
	curr_x = event->globalX();

	if (allowVert and scroll_range_y) {
		// (curr_y - start_y) = delta_pixels
		// scroll_start / scroll_range = fraction_value_of_scrollbar
		// fraction_value_of_scrollbar * scale_y = pixels above widget base
		// pixels above widget base + delta_pixels = new_fraction_value_of_scrollbar * scale
		// new_fraction_value_of_scrollbar = new_value_of_scrollbar / scroll_range
		// v2 = ((scroll_start / scroll_range) * scale + (curr - start))/scale)*scroll_range
		//
		// if scale is the scrollarea height then moving will match the scrollbar
		// 
		// if scale is the child widget height then moving will scroll pixel by pixel
		
		
//		DEBUG(scroll_start_y<<" + (("<<start_y<<" - "<<curr_y<<") * "<<scroll_range_y<<")/"<<scale_y);
		int v_y = scroll_start_y +  direction * ((start_y - curr_y) * scroll_range_y)/scale_y;
		verticalScrollBar()->setValue(v_y);
	}
	if (allowHoriz and scroll_range_x) {
		int v_x = scroll_start_x +  direction * ((start_x - curr_x) * scroll_range_x)/scale_x;
		verticalScrollBar()->setValue(v_x);
	}
	curr_time = event_time.elapsed();
	DEBUG("scroll move " <<curr_x<<":"<<curr_y<<"  time:"<<curr_time);
	
	if (curr_time >	last_ev_time){
		if (last_ev_time){  // first time round we set v to zero
			vel_x1 = vel_x;
			vel_x= (last_x - curr_x)*VSCALE / (curr_time - last_ev_time);
			vel_y1 = vel_y;
			vel_y= (last_y - curr_y)*VSCALE / (curr_time - last_ev_time);
			DEBUG("Velocity last_y:"<<last_y<< " curr_y:"<<curr_y <<"  curr_time:"<<curr_time <<" last_ev_time:"<<last_ev_time << "  Velocity: "<<vel_y);
		} else {
			vel_x = vel_y = 0;
			vel_x1 = vel_y1 = 0;
		}
		// Store the last values
		last_ev_time = 	curr_time;
		last_x = curr_x; 
		last_y = curr_y;
	}
	event->accept();
}
void QFingerScrollArea::mouseReleaseEvent ( QMouseEvent * event )
{
//	_ENTER;
	if (scrolling and event->button() == Qt::LeftButton) {
		event->accept();		
		if (kinetic and last_ev_time) { // Only do velocity if we had a last_ev_time
			curr_time = event_time.elapsed();
#ifdef DEBUG_SHOPPER
			int y = event->globalY();
			int x = event->globalX();
#endif
			DEBUG("scroll stop " <<x<<":"<<y<<"  time:"<<curr_time);

			kinetic_cycles = 0;
			kineticTimer=startTimer(KINETIC_REFRESH);
		}
		scrolling = false;
		scrolling_setup = false;
		recently_scrolling = true;
		debounceTimer = startTimer(DEBOUNCE);
		return;
	}
	QScrollArea::mouseReleaseEvent(event);
}
////////////////////////////////////////////////////////////////
// Kinetics
void QFingerScrollArea::timerEvent(QTimerEvent *event)
{
	if (event->timerId() == debounceTimer) {
		recently_scrolling = false;
		killTimer(debounceTimer);
		debounceTimer = 0;
		return;
	}

	if (event->timerId() == kineticTimer) {
		if (allowVert) {
			int v_y = verticalScrollBar()->value() +
				direction * vel_y1 * KINETIC_REFRESH/VSCALE * scroll_range_y / scale_y;
			verticalScrollBar()->setValue(v_y);
			vel_y1 *= DECEL;
		}

		if (allowHoriz) {
			int v_x = horizontalScrollBar()->value() +
				direction * vel_x1 * KINETIC_REFRESH/VSCALE * scroll_range_x / scale_x;
			horizontalScrollBar()->setValue(v_x);
			vel_x1 *= DECEL;
		}
		show();
		kinetic_cycles++;
		if (scrolling or kinetic_cycles > DECEL_DURATION/KINETIC_REFRESH or
			(vel_x1 == 0 and vel_y1 ==0)) {
			killTimer(kineticTimer);
		}
	}
}

////////////////////////////////////////////////////////////////
// Child management - event filtering
// Handle events down the hierarchy : FIX: as children are added, register them
void QFingerScrollArea::childEvent ( QChildEvent * event )
{
	DEBUG("childEvent is a " << event->type());
	if (event->type() == QEvent::ChildAdded or
		event->type() == 70) {    // FIX is this a Qt bug? 70 is ChildInserted which is deprecated in 4.4
		registerChildrenForFingerScrolling(event->child());
	}
	if (event->removed()) {
		deregisterChildrenForFingerScrolling(event->child());
	}
}

void QFingerScrollArea::registerChildrenForFingerScrolling(QObject *top)
{
	top->installEventFilter(this);
	DEBUG("registered a " << top->metaObject()->className());
	QObject *obj;
	QList<QObject *> children = top->findChildren<QObject *>();
	foreach (obj, children){
		DEBUG("registered a " << obj->metaObject()->className());
		obj->installEventFilter(this);
	}
}
void QFingerScrollArea::deregisterChildrenForFingerScrolling(QObject *top)
{
	top->removeEventFilter(this);
	DEBUG("deregistered a " << top->metaObject()->className());
	QObject *obj;
	QList<QObject *> children = top->findChildren<QObject *>();
	foreach (obj, children){
		obj->removeEventFilter(this);
		DEBUG("deregistered a " << obj->metaObject()->className());
	}
}

bool QFingerScrollArea::eventFilter(QObject *obj, QEvent *event)
{
	switch (event->type()) {
	case QEvent::MouseButtonPress :
		if (recently_scrolling) {
			return true;
		}
		if (static_cast<QMouseEvent *>(event)->button() == Qt::LeftButton){
			setupEvent ( static_cast<QMouseEvent *>(event));
		}
		break;
	case QEvent::MouseButtonRelease :
		if (scrolling) {
			mouseReleaseEvent(static_cast<QMouseEvent *>(event));
			return true;
		}
		if (recently_scrolling) {
			return true;
		}
		break;
	case QEvent::MouseMove :
		if (scrolling) {
			mouseMoveEvent(static_cast<QMouseEvent *>(event));
			return true;
		}
		break;			
	case QEvent::ChildAdded :
		registerChildrenForFingerScrolling(static_cast<QChildEvent *>(event)->child());
		break;
	case QEvent::ChildRemoved :
		deregisterChildrenForFingerScrolling(static_cast<QChildEvent *>(event)->child());
		break;
	default:
		break;
	}
	// standard event processing
	return QScrollArea::eventFilter(obj, event);
}

