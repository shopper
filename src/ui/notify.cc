/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

//#define DEBUG_SHOPPER 1
#include "notify.h"
#include "shopper.h"           // automake, i8n, gettext
#include <QMessageBox>

namespace Shopper
{
	// class Notify : public QSystemTrayIcon
	Notify::Notify(const QIcon & icon) :
		QSystemTrayIcon(icon)
	{}

	void Notify::showMessage ( const QString & title, const QString & message, MessageIcon icon, int millisecondsTimeoutHint )
	{
		if (supportsMessages()) {
			DEBUG("supportsMessages");
			QSystemTrayIcon::showMessage(title, message, icon, millisecondsTimeoutHint );
		} else {
			QMessageBox msgBox;
			msgBox.setText(title);
			msgBox.setInformativeText(message);
			msgBox.exec();
		}
	}
}
