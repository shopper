/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#define DEBUG_SHOPPER 1
#include "mainwindow.h"
#include "shopper.h"           // automake, i8n, gettext

#include "CatListModel.h"
#include "CategoryDialog.h"
#include "ItemDialog.h"
#include "PreferencesDialog.h"
#include <QSettings>

// class MainWindow : public QMainWindow
MainWindow::MainWindow(QString file) :
	mylist(0),
	catCombo(0),
	lv(0),
	filename(file),
	fontsize(4),
	full_screen(false),
	has_rotation(false)
{
	// The constructor handles only 'one off' stuff - main menu,
	// buttons etc. Anything List specific is separated off to
	// handle_list() so it can be reused when opening new lists

	// xrandr detection from adv-backlight...
#ifdef XRANDR
	Display *dpy;
	int xrr_event_base, xrr_error_base;
	int xrr_major, xrr_minor;
	dpy = XOpenDisplay (0);
	if (dpy == 0)
	{
		DEBUG("Couldn't open display\n");
		has_rotation = false;
	} else {
		if (XRRQueryExtension (dpy, &xrr_event_base, &xrr_error_base) == false
			|| XRRQueryVersion (dpy, &xrr_major, &xrr_minor) == 0 || xrr_major != 1 || xrr_minor < 1) {
			DEBUG("Display has no rotation\n");			
			has_rotation = false;
		} else {
			DEBUG("Display has rotation\n");
			has_rotation = true;
		}
		XCloseDisplay (dpy);
	}
#endif
	// Actions
	// UI components
	QAction *addItemAct = new QAction(QIcon("/usr/share/icons/hicolor/64x64/apps/shopper/add.png"),
									  tr("Add Item"), this);
    addItemAct->setStatusTip(tr("Add an item to the list"));
    connect(addItemAct, SIGNAL(triggered()), this, SLOT(on_action_add_item()));

	QAction *manageCatAct = new QAction(QIcon("/usr/share/icons/hicolor/64x64/apps/shopper/categories.png"),
										tr("Manage Categories"), this);
    manageCatAct->setStatusTip(tr("Manage the list of categories"));
    connect(manageCatAct, SIGNAL(triggered()), this, SLOT(on_action_manage_category()));

	QAction *clearWantedAct = new QAction(qApp->style()->standardIcon(QStyle::SP_FileDialogListView),
										  tr("Clear Wanted"), this);
    clearWantedAct->setStatusTip(tr("Clear the items marked as 'wanted'"));
    connect(clearWantedAct, SIGNAL(triggered()), this, SLOT(on_action_clear_wanted()));

	QAction *emptyBasketAct = new QAction(qApp->style()->standardIcon(QStyle::SP_FileDialogListView),
										  tr("Empty Basket"), this);
    emptyBasketAct->setStatusTip(tr("Clear the items marked as 'in the basket'"));
    connect(emptyBasketAct, SIGNAL(triggered()), this, SLOT(on_action_clear_bought()));

	QAction *fullListAct = new QAction(QIcon("/usr/share/icons/hicolor/64x64/apps/shopper/full-list.png"),
									   tr("Out Shopping: Full List"), this);
    fullListAct->setStatusTip(tr("Show the full list (items in the basket will be visible and ticked)"));
    connect(fullListAct, SIGNAL(triggered()), this, SLOT(on_action_fullList()));

	QAction *whatsLeftAct = new QAction(QIcon("/usr/share/icons/hicolor/64x64/apps/shopper/whats-left.png"),
										tr("Out Shopping: What's Left"), this);
    whatsLeftAct->setStatusTip(tr("Show what's left to get"));
    connect(whatsLeftAct, SIGNAL(triggered()), this, SLOT(on_action_whatsLeft()));

	QAction *makingListAct = new QAction(QIcon("/usr/share/icons/hicolor/64x64/apps/shopper/making-list.png"),
										 tr("Making List"), this);
    makingListAct->setStatusTip(tr("Pick items to get when you go shopping"));
    connect(makingListAct, SIGNAL(triggered()), this, SLOT(on_action_makingList()));

	QAction *newAct = new QAction(qApp->style()->standardIcon(QStyle::SP_FileDialogListView),
								  tr("New List"), this);
    newAct->setStatusTip(tr("Add an item to the list"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(on_action_file_new()));

	QAction *openAct = new QAction(qApp->style()->standardIcon(QStyle::SP_DialogOpenButton),
								   tr("Open List"), this);
    openAct->setStatusTip(tr("Open a new list"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(on_action_file_open()));

	QAction *saveAct = new QAction(qApp->style()->standardIcon(QStyle::SP_DialogSaveButton),
								   tr("Save List"), this);
    saveAct->setStatusTip(tr("Save the list"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(on_action_file_save()));

	QAction *saveAsAct = new QAction(qApp->style()->standardIcon(QStyle::SP_DialogSaveButton),
									 tr("Save List As..."), this);
    saveAsAct->setStatusTip(tr("Save the list with a new name"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(on_action_file_saveas()));

	QAction *prefsAct = new QAction(qApp->style()->standardIcon(QStyle::SP_DialogSaveButton),
									tr("Preferences..."), this);
    prefsAct->setStatusTip(tr("Application preferences"));
    connect(prefsAct, SIGNAL(triggered()), this, SLOT(on_action_preferences()));

	QAction *rotateAct = new QAction(QIcon("/usr/share/icons/hicolor/64x64/apps/shopper/rotate.png"),
									 tr("Rotate"), this);
    prefsAct->setStatusTip(tr("Rotate Application"));
    connect(rotateAct, SIGNAL(triggered()), this, SLOT(on_action_rotate()));

	QAction *quitAct = new QAction(qApp->style()->standardIcon(QStyle::SP_BrowserStop),
								   tr("Quit"), this);
    quitAct->setStatusTip(tr("Finish"));
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

	QAction *aboutAct = new QAction(qApp->style()->standardIcon(QStyle::SP_MessageBoxInformation),
									tr("About..."), this);
    aboutAct->setStatusTip(tr("About Shopper..."));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(on_action_about()));

    // Defined in class to be available elsewhere
	nextAct = new QAction(qApp->style()->standardIcon(QStyle::SP_MediaSkipForward),
						  tr("Next Category"), this);
    nextAct->setStatusTip(tr("View the next category"));
    connect(nextAct, SIGNAL(triggered()), this, SLOT(on_action_next()));
	
	QAction *prevAct = new QAction(qApp->style()->standardIcon(QStyle::SP_MediaSkipBackward),
								   tr("Prev Category"), this);
    prevAct->setStatusTip(tr("View the previous category"));
    connect(prevAct, SIGNAL(triggered()), this, SLOT(on_action_prev()));

	// Menus
    menuBar()->addAction(addItemAct);
    menuBar()->addAction(manageCatAct);

	QMenu *clearMenu = menuBar()->addMenu(tr("Clear List"));
    clearMenu->addAction(clearWantedAct);
    clearMenu->addAction(emptyBasketAct);

	QMenu *modeMenu = menuBar()->addMenu(tr("Mode"));
	modeMenu->addAction(fullListAct);
    modeMenu->addAction(whatsLeftAct);
    modeMenu->addAction(makingListAct);

	menuBar()->addSeparator();
	
	QMenu *fileMenu = menuBar()->addMenu(tr("File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(saveAsAct);
	
    menuBar()->addAction(prefsAct);

    QMenu *helpMenu = menuBar()->addMenu(tr("Help"));
    helpMenu->addAction(aboutAct);
	
	menuBar()->addAction(quitAct);

	// Toolbar
    buttonBar = addToolBar(tr("Buttons"));
    buttonBar2 = addToolBar(tr("Navigation"));
	buttonBar->setIconSize(QSize(40,40));
	buttonBar2->setIconSize(QSize(40,40));
    addToolBar(Qt::BottomToolBarArea, buttonBar); // move it to the right area
    addToolBar(Qt::BottomToolBarArea, buttonBar2); // move it to the right area
	buttonBar->addAction(addItemAct);
    buttonBar->addAction(manageCatAct);
    buttonBar->addSeparator();
    buttonBar->addAction(fullListAct);
    buttonBar->addAction(whatsLeftAct);
    buttonBar->addAction(makingListAct);
    buttonBar2->addAction(prevAct);
    buttonBar2->addAction(nextAct);
	buttonBar2->addSeparator();
    buttonBar2->addAction(rotateAct);
	// Status
    statusBar()->showMessage(tr("Ready"));

	QIcon shopper_icon("/usr/share/icons/hicolor/64x64/apps/shopper/shopper.png");
	setWindowIcon(shopper_icon);

	// Prepare our Settings
	QCoreApplication::setOrganizationName("dgreaves.com");
	QCoreApplication::setOrganizationDomain("dgreaves.com");
	QCoreApplication::setApplicationName("Shopper");

	// size and position
    readSettings();
	timerId = startTimer(0);

	QLabel *notice = new QLabel("List Loading. Wait just a moment...", this);
	notice->show();
	setCentralWidget(notice);
	_LEAVE;
}

void MainWindow::timerEvent(QTimerEvent *event)
{
	killTimer(event->timerId());
	qApp->processEvents(); // ensure we're quiet
	qApp->processEvents(); // ensure we're quiet
	loadList();
}

void MainWindow::loadList()
{
	Shopper::List *l;
	if (mylist == 0) {
		l = Shopper::List::from_file(filename);
		if (l) { // If filename valid
			create_list_view(l);
		} else { // If filename wasn't read
			on_action_file_new();
			while (!mylist) { // mylist and list_view are done in a file_open()
				DEBUG("Couldn't open " << filename << " for reading\n");
				on_action_file_open();
				// FIXME :: Popup dialog box and allow new/open
			}
		}
	}
}

void MainWindow::create_list_view(Shopper::List *list)
{
	// Remove the old listview... and any data
	if (mylist) mylist->deleteLater();
	if (lv) lv->deleteLater();
	if (catCombo)
	{
		if (catCombo->model()) delete catCombo->model();
		delete catCombo;
	}
	// and use a list and a new main shopperList widget
	mylist = list;
	lv = new Shopper::ListView(*list, this);
	setCentralWidget(lv);
	// GUI: set title, add the view, update the menu
	setWindowTitle("Shopper:"+mylist->modeText());
	// Make sure there's a menu to show to enable the popup functionality
	
	catCombo = new QComboBox();
	Shopper::CatListModel *clmodel = new Shopper::CatListModel(*mylist);
	catCombo->setModel(clmodel);
	catCombo->setModelColumn(1);
	catCombo->setCurrentIndex(clmodel->currentIndex());
	connect(catCombo, SIGNAL(activated(int)),
			this, SLOT(cat_selected(int)));
	connect(clmodel, SIGNAL(currentChanged(int)),
			catCombo, SLOT(setCurrentIndex(int)));
	buttonBar2->insertWidget(nextAct, catCombo);

	// Update UI when the state changes
	connect(mylist, SIGNAL(state_changed()),
			this, SLOT(stateChanged()));
	
	_LEAVE;
}

void MainWindow::cat_selected(int i)
{
	Q_UNUSED(i)
	_ENTER;
	QVariant v = catCombo->itemData(catCombo->currentIndex());
	Shopper::Category *c = v.value<Shopper::Category*>();
	mylist->make_category_active(*c);
}


void MainWindow::closeEvent(QCloseEvent *event)
{
	_ENTER;
	QSettings settings;
	if (settings.value("data/SaveOnExit").toBool())
		on_action_file_save();
	writeSettings();
	if (has_rotation and settings.value("ui/RotateSafe").toBool()) {
		XRRScreenConfiguration *scr_config;
		int size;
		Display* dpy = XOpenDisplay (NULL);
		int screen = DefaultScreen (dpy);
		scr_config = XRRGetScreenInfo (dpy, RootWindow (dpy, screen));
		size = XRRConfigCurrentConfiguration (scr_config, &rotation);
		XRRSetScreenConfig (dpy, scr_config, RootWindow (dpy, screen), size, RR_Rotate_0, CurrentTime);
		XRRFreeScreenConfigInfo (scr_config);
		XCloseDisplay (dpy);
	}
	event->accept();
}

void MainWindow::on_action_about()
{
   QMessageBox::about(this, tr("About Shopper"),
            tr("<b>Shopper</b> was written by David Greaves (lbt)<br>"
               "It is Free Software and licensed under the GPL.<br>"
               "I hope you enjoy using it.<br>"
			   "Please let me know of any problems or suggestions."));
}


void MainWindow::readSettings()
{
    QSettings settings;
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(400, 400)).toSize();
    filename = settings.value("filename", filename).toString();
    resize(size);
    move(pos);
}

void MainWindow::writeSettings()
{
    QSettings settings;
    settings.setValue("pos", pos());
    settings.setValue("size", size());
    settings.setValue("filename", filename);
	DEBUG("Wrote Settings");
}

// Action handlers
// Add item
void MainWindow::on_action_add_item()
{
	Shopper::ItemAdd itemD(this, mylist);
	if (itemD.exec() == QDialog::Accepted) {
		DEBUG("\nADDED:\n");
	}
}

void MainWindow::on_action_manage_category()
{
	Shopper::CategoryManage catD(this, mylist);
	catD.exec();
}

// Category cycle
void MainWindow::on_action_next(){ 	mylist->make_next_category_active(); }
void MainWindow::on_action_prev(){ 	mylist->make_prev_category_active(); }

// Clear List
void MainWindow::on_action_clear_wanted() { mylist->clear(Shopper::CLEAR_WANTED); }
void MainWindow::on_action_clear_bought() { mylist->clear(Shopper::CLEAR_BOUGHT); }

// Shopping mode
void MainWindow::on_action_fullList() { mylist->set_state(Shopper::OUT_SHOPPING); }
void MainWindow::on_action_whatsLeft() { mylist->set_state(Shopper::WHATS_LEFT); }
void MainWindow::on_action_makingList() { mylist->set_state(Shopper::MAKING_LIST); }

void MainWindow::stateChanged()
{
	setWindowTitle("Shopper:" + mylist->modeText());
}
////////////////////////////////////////////////////////////////
// File Handling
void MainWindow::on_action_file_new()
{
	// FIXME: This really ought to check if the user *WANTS* to save...
	if (mylist)
		on_action_file_save();
	filename="";
	create_list_view(new Shopper::List(
		QString(
			"<list name='Supermarket' state='0'>"
			"<category name='Aisle 1 - Dairy'>"
			"<item desc='Cheese' note='note, no smelly cheese' wanted='1' bought='0'/>"
			"<item desc='Bacon' note='Extra needed for Gary&apos;s butties' wanted='1' bought='0'/>"
			"</category>"
			"<category name='Frozen'>"
			"<item desc='Peas' note='' wanted='1' bought='0'/>"
			"<item desc='Prawns' note='' wanted='1' bought='0'/>"
			"</category>"
			"<category name='Veg'>"
			"<item desc='Potatoes' note='' wanted='0' bought='0'/>"
			"<item desc='Brocolli' note='' wanted='1' bought='0'/>"
			"</category>"
			"<category name='Shoes'>"
			"<item desc='Versace' note='For Tracy' wanted='0' bought='0'/>"
			"</category>"
			"</list>"
			)));
}
void MainWindow::on_action_file_open()
{
	// Get a filename
	QFileDialog dialog(this);
	QString dir = filename.isEmpty() ? QString(HOME_DIR)+ QString(DEFAULT_LIST) : filename ;
	//Handle the response:
	if (!(filename = dialog.getOpenFileName(this, "Select a list",dir, tr("Lists (*)"))).isEmpty()) {
		// Open file and create a view from it.
		create_list_view(Shopper::List::from_file(filename));
		writeSettings(); // FIXME this should be on success
	} else {
		if (mylist == 0) on_action_file_new();  // If CANCEL pressed and there's no list, make a new one
	}

}
////////////////////////////////////////////////////////////////
void MainWindow::on_action_file_save()
{
	DEBUG("Saving....\n");
	if (filename.isEmpty()) {
		on_action_file_saveas();
	} else {
		mylist->to_file(filename);
	}
}
void MainWindow::on_action_file_saveas()
{
	// Get a filename
	QFileDialog dialog(this);
	QString dir = HOME_DIR+(filename==""?DEFAULT_LIST:filename);
	//Handle the response:
	if (!(filename = dialog.getSaveFileName(this, "Save list as", dir, tr("Lists (*.xml)"))).isEmpty()) {
		// Open file and create a view from it.
		writeSettings();
		mylist->to_file(filename);
	}
}
void MainWindow::on_action_preferences()
{
	Shopper::PreferencesDialog prefsD(this);
	prefsD.exec();
	lv->update();
}

void MainWindow::keyPressEvent ( QKeyEvent * event )
{
	switch (event->key())
	{
    case Qt::Key_F7: // Zoom in
		lv->zoomIn();
		break;
    case Qt::Key_F8: // Zoom out
		lv->zoomOut();
		break;
    default:
		QMainWindow::keyPressEvent(event);
		return;
		break;
	}
	return;
}

void MainWindow::on_action_rotate()
{
	if (! has_rotation) return;
	
	XRRScreenConfiguration *scr_config;
//	Rotation current_rotation;
	int size;
//	int i;
	Display *dpy;
	int screen;

	dpy = XOpenDisplay (0);
	screen = DefaultScreen (dpy);
	scr_config = XRRGetScreenInfo (dpy, RootWindow (dpy, screen));
	size = XRRConfigCurrentConfiguration (scr_config, &rotation);

	if (rotation == RR_Rotate_0) {
		QSettings settings;
		if (settings.value("ui/RotateOtherWay",0).toBool()) {
			rotation = RR_Rotate_270;
		} else {
			rotation = RR_Rotate_90;
		}				
		insertToolBarBreak(buttonBar2);
	} else {
		rotation = RR_Rotate_0; // always rotate back to 0
		removeToolBarBreak(buttonBar2);
	}

	XRRSetScreenConfig (dpy, scr_config, RootWindow (dpy, screen), size, rotation, CurrentTime);
	XRRFreeScreenConfigInfo (scr_config);
	XCloseDisplay (dpy);

}

void MainWindow::on_action_help()
{
	
}
