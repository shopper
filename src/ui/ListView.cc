/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

//#define DEBUG_SHOPPER 1
#include "ListView.h"
#include "shopper.h"           // automake, i8n, gettext
#include "CategoryView.h"
#include "ItemDialog.h"
#include "LabelEntry.h"
#include <QHash>
#include <QFrame>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QSettings>
#include <QScrollBar>
#include <QApplication>
#include <iostream>
#include "GestureWatcher.h"

using namespace std;

namespace Shopper
{
// This class displays and manages a list.
//
// It creates ItemViews dynamically
// When the mode changes it updates the display
// When the active category changes, it updates the display
//
	struct ListView::Private {
		QHash<Category*, CategoryView*> catMap;
		Shopper::List        *mylist;
		
		QFrame               *contents; // This widget has a layout and contains all the items
		QVBoxLayout          *lvBox;     // and is scrolled by this widget
		int                  zoom;
	};


// class ListView: public QScrollArea
	ListView::ListView(List &l, QWidget *parent) :
		QFingerScrollArea(parent)
	{
		p = new Private;
		p->mylist =&l;
		p->contents = 0;
		p->lvBox = 0;
		QSettings settings;
		p->zoom = settings.value("ui/ZoomLevel", 4).toInt();
		viewport()->setAutoFillBackground(false);
			
//		setBackgroundRole(QPalette::Light);
		setWidgetResizable(true);
		QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
		sizePolicy.setHorizontalStretch(1);
		setSizePolicy(sizePolicy);
		setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
		setup();
		updateVisibility();

		// Handle changes to the data structure
		connect(&l, SIGNAL(category_added(Category*)),
				this, SLOT(add_cat(Category*)));
		connect(&l, SIGNAL(category_removed(Category*)),
				this, SLOT(del_cat(Category*)));
		connect(&l, SIGNAL(category_list_reordered()),
				this, SLOT(list_reordered()));
		
		// Now connect the List's changed signals
		// When the mode changes
		connect(&l, SIGNAL(state_changed()),
				this, SLOT(list_changed()));

		// When the active category changes
		connect(&l, SIGNAL(active_category_changed()),
				this, SLOT(active_cat_changed()));

		// Issued on name change
		connect(&l, SIGNAL(changed()),
				this, SLOT(list_changed()));
	}

	ListView::~ListView()
	{
		delete p;
	}

	void ListView::filter_by(Category &c)  // Only view certain categories
	{
		p->mylist->make_category_active(c);
	}

	void ListView::list_reordered()
	{
		setup();
	}
	void ListView::active_cat_changed()
	{
		updateVisibility();
		QScrollBar *vs = verticalScrollBar();
		if (vs) vs->setValue(vs->minimum());
	}
	void ListView::list_changed()
	{
		_ENTER;
		updateVisibility();
	}
	
    // FIX: This should specify position
	void ListView::add_cat(Shopper::Category *c)
	{
//		CategoryView *cw;
//		cw = new CategoryView(*c, *(p->mylist), p->contents);
//		p->lvBox->addWidget(cw, 1);
//		p->catMap[c] = cw;
		Q_UNUSED(c)
		setup(); // FIX: change Shopper::List to be based on QList and do this properly
	}

	void ListView::del_cat(Shopper::Category *c)
	{
		QWidget *w = p->catMap[c];
		p->lvBox->removeWidget(w);
		p->catMap.remove(c);
		delete w;
		updateVisibility(); // FIX: change Shopper::List to be based on QList and do this properly
	}

	// Creates the structure of view widgets
	void ListView::setup()
	{
		QVBoxLayout* oldBox = p->lvBox;

		p->contents = new QFrame;  // don't worry, the old value is destroyed by setWidget() in a bit...
		p->lvBox = new QVBoxLayout;
		p->contents->setLayout(p->lvBox);
		p->contents->setLineWidth(3);

		// Scans all the categories in the Shopper::List and creates views
		for(Shopper::List::pCategoryIter cI = p->mylist->categoriesI();
			cI != p->mylist->categoriesEnd(); ++cI)
		{
			Shopper::Category *c = *cI;
			{
				CategoryView *cw;
				if (p->catMap.contains(c)) {
					cw = p->catMap[c];
					cw->setParent(p->contents);
					cw->show();
					DEBUG("Existing CategoryView for " << c->get_name());
				} else {
					cw = new CategoryView(*c, *(p->mylist), p->contents);
					p->catMap[c] = cw;
					cw->setZoom(p->zoom);
					DEBUG("New CategoryView for " << c->get_name());
				}
				if (oldBox) oldBox->removeWidget(cw);
				p->lvBox->addWidget(cw, 1);
			}
		}

		p->lvBox->addStretch(1);
		QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
		sizePolicy.setHorizontalStretch(1);
		p->contents->setSizePolicy(sizePolicy);
		setWidget(p->contents); // deletes the old widget and anything left in it...
		qApp->sendPostedEvents();
		qApp->processEvents();
		qApp->processEvents();
	}
	
	// iterate through all widgets and hide/show them
	void ListView::updateVisibility()
	{
		_ENTER;
		CategoryView *cw;
		p->contents->hide();
		for(Shopper::List::pCategoryIter cI = p->mylist->categoriesI();
			cI != p->mylist->categoriesEnd(); ++cI)
		{
			Shopper::Category *c = *cI;
			cw = p->catMap[c];
			bool isActive = p->mylist->is_category_active(*c);
			if (isActive) {
				cw->updateVisibility();
				cw->show();
			} else {
				cw->hide();
			}
		}
		p->contents->show();
	}

#define MAXZOOM 6
	void ListView::setZoom(int z)
	{
		if (z < 0) return;
		if (z > MAXZOOM) return;
		p->zoom=z;
		QSettings settings;
		settings.setValue("ui/ZoomLevel", z);
		int max1 = verticalScrollBar()->maximum();
		int s = verticalScrollBar()->value();
		DEBUG("Scrolled to " << s << "/" <<max1);
		for(Shopper::List::pCategoryIter cI = p->mylist->categoriesI();
			cI != p->mylist->categoriesEnd(); ++cI)
		{
				p->catMap[*cI]->setZoom(p->zoom);
		}
		qApp->sendPostedEvents();
		qApp->processEvents();
		qApp->processEvents();
		int max2 = verticalScrollBar()->maximum();
//		DEBUG("given " << max2 << " going to " << s*max2/max1);
		if (max1) verticalScrollBar()->setValue(s*max2/max1);
	}
	void ListView::zoomOut() { setZoom(p->zoom-1); }
	void ListView::zoomIn() { setZoom(p->zoom+1); }
}

