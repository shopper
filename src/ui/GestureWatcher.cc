/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

//#define DEBUG_SHOPPER 1
#include "GestureWatcher.h"
#include "shopper.h"           // automake, i8n, gettext
#include <QHash>
#include <QQueue>
#include <QPointer>
#include <QWidget>
#include <QChildEvent>
#include <QTime>
#include <QApplication>



////////////////////////////////////////////////////////////////
// Class and data structure setup

struct GestureWatcher::Watched {
	typedef enum _w { No = 0, Yes, Child } watchStatus;
};
struct GestureWatcher::WidgetInfo
{
	Watched::watchStatus status;
	QPointer<QWidget> watchedParent; // the parent that is actually registered
	                                 // only set if status==AParentWatched

	// This is fiddly, we use a Gesture as the hash because
	// qHash(Gesture) hashes on the QString code.  That way if we get
	// multiple connect()s to a single gesture, they all go to the
	// same Gesture	
	QHash<Gesture, Gesture*> registeredGestures;
};

struct GestureWatcher::Private
{
	bool gesturing_maybe;
	bool gesturing;
	bool gesturing_recently;
	bool reissuing_events;
	QPointer<QWidget> gesturing_widget;
	QHash<QWidget*, WidgetInfo*> watchlist;
	int start_x;
	int start_y;
	int last_ev_time;
	bool setup;
	QTime event_time;
	int replayEventTimer;
	int gestureNotStartedTimer;
	int multiStrokeTimer;
	GestureReader *reader;
	QQueue<QEvent*> storedEvents;

};

const int GestureWatcher::SENSITIVITY = 20;
const int GestureWatcher::MULTISTROKE = 200; // time after a release to allow a
								 // new stroke to start. Need to
								 // balance responsiveness with enough
								 // time to make gesture
const int GestureWatcher::START = 100; // We need a stroke within this many milliseconds...
// protected -- The singleton constructor
GestureWatcher::GestureWatcher()
{
	p = new Private;
	p->	gesturing_maybe = false;
	p->gesturing = false;
	p->gesturing_recently = false;
	p->reissuing_events = false;
	p->reader = new GestureReader();
	clearEvents();
}

GestureWatcher::~GestureWatcher()
{
	QHash<QWidget*, WidgetInfo*>::const_iterator wi = p->watchlist.constBegin();
	while (wi != p->watchlist.constEnd()) {
		foreach (Gesture *gi, p->watchlist[wi.key()]->registeredGestures) {
			delete gi;
		}
		p->watchlist[wi.key()]->registeredGestures.clear();
		++wi;
	}
	p->watchlist.clear();
	delete p->reader;
	delete p;
}

////////////////////////////////////////////////////////////////
// public interface
GestureWatcher* GestureWatcher::theWatcher = 0;
GestureWatcher* GestureWatcher::getGestureWatcher()  // Get the singleton
{
	_ENTER;
	return theWatcher ?
		theWatcher :
		theWatcher = new GestureWatcher;
}


////////////////////////////////////////////////////////////////
// Handle the main connect() call and associated registration
//
bool GestureWatcher::connect(QWidget *w, const Gesture &g,
			 QObject *receiver, const char * method)
{
	_ENTER;
	Q_ASSERT(w && receiver && method);
	if (! (w && receiver && method)) return false;
	
	if (! p->watchlist.contains(w)) {  // If we're not watching this widget already ....
		registerHeirarchyForGestures(w);
	}

	// We're (now) watching this widget so we can connect it...
	Gesture *myg;
	if (p->watchlist[w]->registeredGestures.contains(g)) {
		myg = p->watchlist[w]->registeredGestures[g];  // we already connected a gesture like this
	} else {
		myg = new Gesture(g);  // make a copy
	}
	myg->connect(receiver, method); // record the connection privately
	p->watchlist[w]->registeredGestures[*myg]=myg; // add it to the list
	return true;
}

bool GestureWatcher::disconnect(QWidget *w, const Gesture &g, QObject *receiver, const char * method)
{
	_ENTER;
	if (!w) return false;

	if (p->watchlist.contains(w) and p->watchlist[w]->status == Watched::Child )
	{
		qWarning("GestureWatcher: Tried to disconnect the child of a connected widget.");
		return false;
	}
	bool res = false;
	if (p->watchlist.contains(w) and p->watchlist[w]->status == Watched::Yes ) // is it being watched
	{
		if (g.shape().isEmpty()){ // Null gesture
			foreach (Gesture *gi, p->watchlist[w]->registeredGestures) {
				delete gi;
				res = true;
			}
			p->watchlist[w]->registeredGestures.clear();  // probably more efficient
														  // than many removes
		} else {
			foreach (Gesture *gi, p->watchlist[w]->registeredGestures) {
				if (g == *gi) {
					// If we have no receiver then delete
					if (!receiver)
					{					
						p->watchlist[w]->registeredGestures.remove(gi);
						delete gi;
					} else {
						res = gi->disconnect(receiver, method) || res; // watch shortcutting!
					}
					break;    // found - so avoid needless looping
				}
			}
		}
		if (p->watchlist[w]->registeredGestures.isEmpty()) {
			// unregister all events
			p->watchlist.remove(w);
			deregisterHeirarchyForGestures(w);
		}
		return res;
	}
	return false;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//protected
////////////////////////////////////////////////////////////////
// Manage registration for monitored hierarchy
void GestureWatcher::registerHeirarchyForGestures(QObject *top)
{
	_ENTER;
	Q_ASSERT(! p->watchlist.contains(static_cast<QWidget*>(top)) );  // Must not be watched
	WidgetInfo* wi = new WidgetInfo;
	wi->status = Watched::Yes;
	p->watchlist[static_cast<QWidget*>(top)] = wi; // watch properly
	top->installEventFilter(this);
	DEBUG("registered a " << top->metaObject()->className());
	QObject *obj;
	foreach (obj, top->findChildren<QObject *>()){
		Q_ASSERT(! p->watchlist.contains(static_cast<QWidget*>(obj)));   // Must not be watched
		wi = new WidgetInfo;
		wi->status = Watched::Child;
		wi->watchedParent=static_cast<QWidget*>(top);
		p->watchlist[static_cast<QWidget*>(obj)] = wi; // watch as a child
		obj->installEventFilter(this);
		DEBUG("registered a " << obj->metaObject()->className());
	}
}

void GestureWatcher::deregisterHeirarchyForGestures(QObject *top)
{
	_ENTER;
	WidgetInfo* wi = p->watchlist[static_cast<QWidget*>(top)];
	Q_ASSERT(wi->status == Watched::Yes || wi->status == Watched::Child);  // Must be watched
	delete wi;
	p->watchlist.remove(static_cast<QWidget*>(top));
	top->removeEventFilter(this);
	QObject *obj;
	foreach (obj, top->findChildren<QObject *>()){
		wi = p->watchlist[static_cast<QWidget*>(obj)];
		Q_ASSERT(wi->status == Watched::Child);      // Must be watched as a child
		delete wi;
		p->watchlist.remove(static_cast<QWidget*>(obj));
		obj->removeEventFilter(this);
	}
}



////////////////////////////////////////////////////////////////
// Event handling
// private
// true = we handled event
// false = we didn't (and may get it again from another widget in chain)
bool GestureWatcher::eventFilter(QObject *obj, QEvent *event)
{
	if (p->reissuing_events) {
		DEBUG("Reissue");
		return QObject::eventFilter(obj, event); // ignore events during re-issue
	}
	// FIX: at some point maybe we should allow gesturing with a mouse with another button
	// current implementation is very finger oriented - or maybe detecting pressure/finger
	// see https://wiki.maemo.org/Using_touch_screen_pressure_data
//	if (mEev->button() == Qt::LeftButton){
	
	// FIX need to do this conditionally
	QMouseEvent * mEv = static_cast<QMouseEvent *>(event);
	QWidget *w = dynamic_cast<QWidget *>(obj); // This will only work if we only filter QWidgets...

	switch (event->type()) {

		// A press always takes us to gesturing_maybe
	case QEvent::MouseButtonPress :
		DEBUG("Button Press");
		if (! p->gesturing) {
			// A new gesture is starting - remove any queued events and note values
			clearEvents();
			p->storedEvents.enqueue(new QMouseEvent(*mEv)); // store the event for maybe replaying
			p->gesturing_maybe = true;
			p->gesturing_widget = w;
			p->start_y = mEv->globalY();
			p->start_x = mEv->globalX();
			p->event_time.start();
			p->last_ev_time=0;
			// Now give them a few ms to start moving the mouse/finger or it's not a gesture
			p->gestureNotStartedTimer = startTimer(START);
		} else { // we are gesturing and this is a second stroke...
			killTimer(p->multiStrokeTimer);
			p->reader->addPoint( mEv->globalX(), mEv->globalY(), GestureReader::Point::Press );
		}
		event->accept();
		return true;
		break;

	case QEvent::MouseButtonRelease :
		DEBUG("Button Release");
		// If we are definitely gesturing then this could be a wrap
		if (p->gesturing) {
			p->multiStrokeTimer = startTimer(MULTISTROKE);
			p->reader->addPoint( mEv->globalX(), mEv->globalY(), GestureReader::Point::Release );
			mEv->accept();
			return true;
		}
		// If we are maybe gesturing then there wasn't a gesture and we force playback
		// from a timer (not recursing down the event chain)
		if (p->gesturing_maybe) {
			DEBUG("Maybe");
			p->storedEvents.enqueue(new QMouseEvent(*mEv));
			p->replayEventTimer = startTimer(0);
			mEv->accept();
			return true ;
		}
		// There's no gesturing going on... this happens on gesture timeout
		return false;
		break;
		
	case QEvent::MouseMove :
		if (p->gesturing_maybe) {
			p->storedEvents.enqueue(new QMouseEvent(*mEv)); // we may need to play this back...
			// if we're maybe gesturing and we've moved enough then declare a start...
			if (abs(p->start_x - mEv->globalX()) > SENSITIVITY ||
				abs(p->start_y - mEv->globalY()) > SENSITIVITY) {
				clearEvents(); // not interested, all previous events are ours
				killTimer(p->gestureNotStartedTimer); // it has...
				DEBUG("\n\nStart Gesturing\n\n");
				p->gesturing_maybe = false;
				p->gesturing = true;
				p->reader->start(p->start_x, p->start_y);
				// now fall through to handling the move
			}
		}
		if (p->gesturing) {
			p->reader->addPoint( mEv->globalX(), mEv->globalY(), GestureReader::Point::Move );
			mEv->accept();
			return true;
		}
		// These move events don't fall between press/release - so pass them on without hindrance
		return false ;					
		
		break;
		
	case QEvent::ChildAdded :
		DEBUG("Child Added");
		registerHeirarchyForGestures(static_cast<QChildEvent *>(event)->child());
		break;
		
	case QEvent::ChildRemoved :
		DEBUG("Child Removed");
		deregisterHeirarchyForGestures(static_cast<QChildEvent *>(event)->child());
		break;
		
	default:
		break;
	}
	// standard event processing
	DEBUG("Event passed");
	return QObject::eventFilter(obj, event);
}

void GestureWatcher::timerEvent(QTimerEvent *event)
{

	// Button pressed and released but not enough movement, replay the events and ignore any subsequent movement.
	if (event->timerId() == p->replayEventTimer) {
		DEBUG("Replay Timer");
		killTimer(p->replayEventTimer);
		p->gesturing_maybe = false;
		p->gesturing = false;
		replayEvents();
		return;
	}

	// Button pressed but no movement, replay the events and ignore any subsequent movement.
	if (event->timerId() == p->gestureNotStartedTimer) {
		DEBUG("Not Started Timer");
		killTimer(p->gestureNotStartedTimer);
		p->gesturing_maybe = false;
		p->gesturing = false;
		replayEvents();
		return;
	}

	// A release occurred but no second stroke - so this is a gesture
	if (event->timerId() == p->multiStrokeTimer) {
		DEBUG("\n\n Stopped Gesturing\n\n");
		killTimer(p->multiStrokeTimer);
		p->gesturing_maybe = false;
		p->gesturing = false;
		
		clearEvents(); // not interested, all previous events are ours

		QPointer<QWidget> w;
		w = p->gesturing_widget;
		if (w.isNull()) return;
		// Now see if the widget or its watchedParent has a matching gesture.
		if (p->watchlist[w]->status == Watched::Child) {
			w = p->watchlist[w]->watchedParent;
		}
		if (w.isNull()) return;
		
		Gesture g = p->reader->matchOne(p->watchlist[w]->registeredGestures.keys() );
		if (p->watchlist[w]->registeredGestures.contains(g)) {
			DEBUG("A Gesture matched");
			Gesture *rich_g = p->watchlist[w]->registeredGestures[g];
			rich_g->invoke();
		} else {
			DEBUG("No Gesture matched");
		}
	}
}

void GestureWatcher::clearEvents()
{
	QEvent *ev;
	while (!(p->storedEvents.isEmpty())) {
		ev = p->storedEvents.dequeue() ;
		delete ev;
	}
}
void GestureWatcher::replayEvents()
{
	QEvent *ev;
	p->reissuing_events = true;
	while (!p->storedEvents.isEmpty()) {
		ev = p->storedEvents.dequeue() ;
		if (! p->gesturing_widget.isNull()) {
			qApp->notify(p->gesturing_widget, ev);
		}
		delete ev;
	}
	p->reissuing_events = false;
}
