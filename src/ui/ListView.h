/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef SHOPPER_UI_LISTVIEW_H
#define SHOPPER_UI_LISTVIEW_H

#include "shopperList.h"
#include "LabelEntry.h"
#include "ItemView.h"
#include "QFingerScrollArea.h"
#include <QVBoxLayout>
#include <QFrame>

using namespace std;
namespace Shopper
{
// This class displays and manages a list.
	class ListView: public QFingerScrollArea
	{
		Q_OBJECT;
	public:
		ListView(Shopper::List &l, QWidget *parent);
		~ListView();

		void filter_by(Shopper::Category &c);  // Only view certain categories
		void updateVisibility();
		
	private:
		void setup(); // should be private but show_all() is buggy AFAICT
		void setZoom(int);

		struct Private;
		struct Private       *p;
		
	public slots:
		// signal handlers
		void list_changed();
		void active_cat_changed();
		void list_reordered();
		void zoomIn();
		void zoomOut();
		void add_cat(Category*);
		void del_cat(Category*);
//		void add_item(Item*);
//		void del_item(Item*);
	};
}

#endif // SHOPPER_UI_LISTVIEW_H
