/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef SHOPPER_UI_ITEMDIALOG_H
#define SHOPPER_UI_ITEMDIALOG_H
#include "shopperList.h"
#include <QDialog>
#include "CatListModel.h"

class QWidget;
class QLabel;
class QLineEdit;
class QLabel;
class QComboBox;
class QPushButton;

namespace Shopper
{
// private base for adding/editing an Item
	class ItemDialog : public QDialog
	{
		Q_OBJECT;
	protected:
		ItemDialog(QWidget * parent, Shopper::List*, Shopper::Item*);
		~ItemDialog();

		// accessor
		Shopper::Category*  get_category();
	
		// Data
		Shopper::List               *mylist;
		Shopper::Item               *myitem;

		CatListModel                *model;
		// State
		int                         catSelected;

		// GUI
		QLabel                  *descLabel;
		QLineEdit               *desc_e;
		QLabel                  *catLabel;
		QComboBox               *catCombo;
		QPushButton             *plusButton;

//		int update_combo();
	public slots:
		void plus_category();
		void add_or_edit();
	};
	class ItemEdit : public ItemDialog
	{
	public:
		ItemEdit(QWidget * parent, Shopper::List* mylist, Shopper::Item* it);
	};
	class ItemAdd : public ItemDialog
	{
	public:
		ItemAdd(QWidget * parent, Shopper::List* mylist);
	};

};
#endif
