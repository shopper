/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

//#define DEBUG_SHOPPER 1
#include "CategoryView.h"
#include "shopper.h"           // automake, i8n, gettext
#include "shopperList.h"
#include "ItemView.h"
#include "LabelEntry.h"
#include "GestureWatcher.h"
#include <QHash>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QSettings>
#include <QFont>
//#include <QFrame>
#include <iostream>
#include <algorithm>

using namespace std;

namespace Shopper
{

	struct CategoryView::Private {
		Category        *mycat;      /* the category being displayed */
		List            *mylist;     /* the parent list */
		
		QHash<Item*, ItemView*> itemMap;

		QWidget         *cvHead;     /* This displays the category description */
		QImage          *cvExpandImg;
		QLabel          *cvName;
		QWidget         *cvList;
		QVBoxLayout     *cvBox;

		int             expanded;
		int             zoom;
	};

// The widget shows the state of an item.
	// class CategoryView: public QWidget
	CategoryView::CategoryView(Category &cat, List &l, QWidget *parent) :
		QWidget(parent)
	{
		p = new Private;
		p->mycat = &cat;
		p->mylist = &l;
		p->zoom = 0;


		QPalette pName = palette();
		pName.setColor(QPalette::WindowText, QColor(255,255,255));
/*
		QLinearGradient bgGrad = QLinearGradient();
		bgGrad.setCoordinateMode(QGradient::ObjectBoundingMode);
		bgGrad.setColorAt(0.0, QColor(132,143,150));
		bgGrad.setColorAt(0.5, QColor(85,104,121));
		bgGrad.setColorAt(0.50, QColor(16,50,71));
		bgGrad.setColorAt(1.0, QColor(176,191,201));
		pName.setBrush(QPalette::Window, bgGrad);
*/

		QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
		sizePolicy.setHorizontalStretch(1);
		sizePolicy.setVerticalStretch(0);
		setSizePolicy(sizePolicy);
//		setFrameStyle(QFrame::Panel|QFrame::Sunken);

		p->cvName = new QLabel(p->mycat->get_name(),this);
		// Lay them out
		p->cvBox = new QVBoxLayout();
		p->cvBox->addWidget(p->cvName);
		p->cvBox->setContentsMargins(0, 0, 0, 0);
		p->cvName->setStyleSheet("background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 rgb(132,143,150), stop: 0.5 rgb(85,104,121), stop: 0.5001 rgb(16,50,71), stop:1.0 rgb(176,191,201) )");

		p->cvName->setAlignment(Qt::AlignLeft);
		p->cvName->setSizePolicy(sizePolicy);
		
		p->cvName->setAutoFillBackground(true);
		p->cvName->setPalette(pName);
		

		// Add the items
		setup();

		// update visibility
		updateVisibility();

		// and apply
		setLayout(p->cvBox);

		// Handle changes to the data structure
		connect(p->mycat, SIGNAL(item_added(Item*)),
				this, SLOT(add_item(Item*)));
		connect(p->mycat, SIGNAL(item_removed(Item*)),
				this, SLOT(del_item(Item*)));

		// Now connect the item's changed signal to this object's updater
		connect(p->mycat, SIGNAL(changed()),
				this, SLOT(category_changed()));
		show();
	}
	
	CategoryView::~CategoryView()
	{
		delete p;
	}
	
	void CategoryView::add_item(Item *i)
	{
		// FIX: do this properly...
		Q_UNUSED(i);
		_ENTER;
		setup();
	}
	
	void CategoryView::del_item(Item *i)
	{
		ItemView *iv = p->itemMap[i];
		p->cvBox->removeWidget(iv);
		iv->deleteLater();
		p->itemMap.remove(i);
	}

	void CategoryView::minimise()
	{
	}
	void CategoryView::maximise()
	{
	}
	void CategoryView::setup()
	{
		ItemView *iv;
		foreach(iv, p->itemMap) {
			p->cvBox->removeWidget(iv);
		}
		for(Shopper::Category::pItemIter itemI = p->mycat->itemsI();
			itemI != p->mycat->itemsEnd(); ++itemI)	{
			if (! p->itemMap.contains(*itemI)) {		
				iv = new ItemView(**itemI, *(p->mylist), this);
				iv->setZoom(p->zoom);
				// Keep track of our child widgets
				p->itemMap[*itemI] = iv;
			} else {
				iv = p->itemMap[*itemI];
			}
			// Pack from the start...
			p->cvBox->addWidget(iv, 1);
		}
	}

	void CategoryView::updateVisibility()
	{
		QSettings settings;
		bool showCategoryHeadings = settings.value("ui/ShowCategoryHeadings").toBool();
		// bool showTwoColumns = settings.value("ui/ShowTwoColumns").toBool();

		if (showCategoryHeadings)
			p->cvName->show();
		else
			p->cvName->hide();
		
		bool isActive = p->mylist->is_category_active(*(p->mycat));
		for(Shopper::Category::pItemIter itemI = p->mycat->itemsI();
			itemI != p->mycat->itemsEnd(); ++itemI)	{
			ItemView *w;
			if (! p->itemMap.contains(*itemI)) {
				DEBUG("No widget for " << (*itemI)->get_desc());
			} else {
//				Item* i = *itemI;
//				DEBUG(
//					(i->get_bought()?"bought":"      ") << " : " <<
//					(i->get_wanted()?"wanted":"      ") << " : " <<
//					i->get_desc());
				w = p->itemMap[*itemI];
				if (isActive)
					w->updateVisibility();
				else
					w->hide();
			}
		}
	}
	void CategoryView::category_changed()
	{
		p->cvName->setText(p->mycat->get_name());
		setup(); // This will ensure all widgets are in the correct order
		show();
	}
	
	void CategoryView::setVisible(bool vis)
	{
		// Hide if not wanted
		if (((p->mylist->get_state() == WHATS_LEFT) || (p->mylist->get_state() == OUT_SHOPPING))
			&& p->mycat->wanted() == 0) {
			DEBUG(" shopping, none wanted, hiding " << p->mycat->get_name());
			QWidget::setVisible(false);
			return;
		}		

		if ((p->mylist->get_state() == WHATS_LEFT) && p->mycat->bought()==p->mycat->wanted()) {
			DEBUG(" what's left + all bought, hiding " << p->mycat->get_name());
			QWidget::setVisible(false);
			return;
		}
		if (vis && p->mylist->is_category_active(*(p->mycat))) {
			DEBUG("Active: Updating and showing " << p->mycat->get_name());
			updateVisibility();
			QWidget::setVisible(vis);
			return;
		}
		QWidget::setVisible(false);
		return;
	}
const int CategoryView::BASEFONTSIZE = 14;
const int CategoryView::MINFONTSIZE = 14;
const int CategoryView::MAXFONTSIZE = 21;

	void CategoryView::setZoom(int z)
	{
		p->zoom = z;
		QFont font = p->cvName->font();
		int s = BASEFONTSIZE+z;
 		if (s < MINFONTSIZE) s=MINFONTSIZE;
 		if (s > MAXFONTSIZE) s=MAXFONTSIZE;
		font.setPointSizeF(s);
		p->cvName->setFont(font);
		for(Shopper::Category::pItemIter itemI = p->mycat->itemsI();
			itemI != p->mycat->itemsEnd(); ++itemI)	{
			p->itemMap[*itemI]->setZoom(z);
		}
	}
}
