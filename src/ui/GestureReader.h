/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef GESTUREREADER_H
#define GESTUREREADER_H

#include "shopper.h"           // automake, i8n, gettext
#include "Gesture.h"

// This class is used to record gesture events
// These can be press, release and move
// as some gestures may have multiple strokes

class GestureReader
{
public:
	class Point
	{
	public:
		typedef enum _Type { Move, Release, Press, Start, Invalid } Type;
		typedef enum _Direction { None,
					   Up, Right, Down, Left,
					   UpRight, DownRight, DownLeft, UpLeft } Direction;

		Point();
		Point( int _x, int _y, int _len, Point::Direction _dir, Point::Type _type);
		
	private:
		friend class GestureReader;
		int x;
		int y;
		int len2; // length squared
		Direction direction;
		Type type;
	};

public:
	GestureReader();
    ~GestureReader();

	void start(int x, int y);
	void addPoint(int x, int y, Point::Type type);

	Gesture matchOne( QList<Gesture> gs);

	
private:
	QString generateStrokes();

	struct Private;
	Private *p;
};

#endif //GESTUREREADER_H
