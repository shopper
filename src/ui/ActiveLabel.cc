/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1
#include "ActiveLabel.h"
#include "shopper.h"           // automake, i8n, gettext
#include "GestureWatcher.h"

namespace Shopper
{
	// ActiveLabel a label that emits pressed() when clicked
	ActiveLabel::ActiveLabel(QWidget * parent, Qt::WindowFlags f ) :
		QLabel(parent, f)
	{
	}
	ActiveLabel::ActiveLabel( const QString & text, QWidget * parent, Qt::WindowFlags f ) :
		QLabel(text, parent, f)
	{
	}

	void ActiveLabel::mouseReleaseEvent ( QMouseEvent * event )
	{
		if (event->button() == Qt::LeftButton) {
			event->accept();
			emit pressed();
		}
		event->ignore();
	}
}
