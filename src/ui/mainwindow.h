/* Shopper
 * Copyright (C) 2009 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>

#define XRANDR 1
#ifdef XRANDR
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#endif

#include "shopperList.h"
#include "ListView.h"
#include "CatListModel.h"

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QTextEdit;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
	MainWindow(QString file);
	void timerEvent(QTimerEvent *event);
	
protected:
	void loadList();
	
	Shopper::List *mylist;    // Main list that we're acting for

	// Member UI helpers and components available to multiple methods
	QComboBox                *catCombo;
	Shopper::ListView        *lv;
	QString                   filename;
	QAction                  *nextAct;
	QToolBar                 *buttonBar;
	QToolBar                 *buttonBar2;
	int                       timerId;
	int                       fontsize;
	
	/* Fullscreen mode is on (TRUE) or off (FALSE) */
	bool                      full_screen;
	bool                      has_rotation;
	Rotation                  rotation;  // X type

	// Gui layout
	void create_list_view(Shopper::List*);


									
protected slots:
	// Main UI action handlers
	// Dialog initiators
	void on_action_add_item();
	void on_action_manage_category();

	// Change current category
	void on_action_next();
	void on_action_prev();

	// List state manipulation
	void on_action_clear_wanted();
	void on_action_clear_bought();
	void on_action_fullList();
	void on_action_whatsLeft();
	void on_action_makingList();

	// File handling
	void on_action_file_new();
	void on_action_file_open();
	void on_action_file_save();
	void on_action_file_saveas();
	void on_action_preferences();

	// Help
	void on_action_help();
	void on_action_about();

	//Signal handlers:
	void keyPressEvent ( QKeyEvent * event );
	void on_action_rotate();
	void closeEvent(QCloseEvent *event);

	// Update UI on data change
	void cat_selected(int i);
	void stateChanged();

private:
    void readSettings();
    void writeSettings();
};

#endif //MAINWINDOW_H
