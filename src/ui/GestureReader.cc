/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1
#include "GestureReader.h"
#include "shopper.h"           // automake, i8n, gettext

// This class is used to record gesture events
//
// There are 2 main functions:
//
// Recording
// =========
// This records a path of points
// These can be press, release and move
// as some gestures may have multiple strokes
//
// Matching
// ========
// Based on an idea by Johan Thelin
//
// smooths/dejitters the points on entry
// Then creates a stroke pattern
// Then matches against the supplied list
//
// If this fails then the stroke pattern is simplified
//

#define STROKECODELEN 4  // length of code strings including \0 (pad short codes with \0s, not ' ')
// Lookup table for GestureReader::Point::Direction
static const char* StrokeCodes =". \0\0u \0\0r \0\0d \0\0l \0\0ur \0dr \0dl \0ul \0";
// Lookup table for GestureReader::Point::pointType
//static const char* TouchCodes =".. \0R \0\0P \0\0* \0\0. \0\0";  // release/press


GestureReader::Point::Point():
	x(0),
	y(0),
	len2(0),
	direction(None),
	type(Invalid) {};

GestureReader::Point::Point( int _x, int _y, int _len2,
							 Point::Direction _dir, Point::Type _type) :
	x(_x), y(_y), len2(_len2), direction(_dir), type(_type)
{}

struct GestureReader::Private
{
	QList<Point> points;  // this is replaced during smoothing
	QString strokes;
	int lastX, lastY;
	int smallestStrokeLen;  // calculated during generateStrokes...
	int SmoothLimit2;
};

// Fwd declare local private helper
GestureReader::Point::Direction calcDirection(int dx, int dy);

// class GestureReader : public QObject
// public:
GestureReader::GestureReader()
{
	p = new Private;
	p->SmoothLimit2 = 900; // 30^2 - no segments < 30 pixels long
}

GestureReader::~GestureReader()
{
	delete p;
}

////////////////////////////////////////////////////////////////
// Creating a path
void GestureReader::start(int x, int y)
{
	_ENTER;
	p->points.clear();
	p->points.append( Point(x, y, 0, Point::None, Point::Start) );
	p->lastX = x;
	p->lastY = y;
}

void GestureReader::addPoint(int x, int y, Point::Type type)
{
//	_ENTER;
	int dx = x - p->lastX;
	int dy = y - p->lastY;
	int len2 = dx*dx + dy*dy;
	if (len2 < p->SmoothLimit2) { // ignore jitter
		// If we jittered to an Release/Press then use the last X/Y
		if (type == Point::Press || type == Point::Release)
			p->points.append( Point(p->lastX, p->lastY, 0, Point::None, type) );
		return;
	}

	// Ensure all movement is recorded using Point::Move and up/down is static.
	p->points.append( Point(x, y, len2, calcDirection( dx, dy), Point::Move) );
	
	// Movement registered and recorded.
	p->lastX = x;
	p->lastY = y;
	
	// an Release/Press if needed
	if (type == Point::Press || type == Point::Release)
		p->points.append( Point(x,y,0, Point::None, type) );
	
}

////////////////////////////////////////////////////////////////
// silly helper functions
int abs(int Nbr){ return (Nbr >= 0 ? Nbr : -Nbr); }

int sqr(int Nbr)
{
	if (Nbr == 1) return 1;
	Nbr = abs(Nbr);
	int Number = Nbr / 2;
	int lastGuess;
	do {lastGuess = Number;	Number = (Number + Nbr / Number) / 2;
	} while( abs(Number - lastGuess) > 1);
	return Number;
}

////////////////////////////////////////////////////////////////
// Analyse points

GestureReader::Point::Direction calcDirection(int dx, int dy)
{
//	_ENTER;
	// Handle the 0 cases to avoid problems....
	if (dx == 0){
		if (dy > 0)
			return GestureReader::Point::Down;
		else
			return GestureReader::Point::Up;
	} else if (dy == 0) {
		if (dx > 0)
			return GestureReader::Point::Right;
		else
			return GestureReader::Point::Left;
	}

	//  tan(theta) = opposite / adjacent
	//  scale by 10000 for a little accuracy - it's rough but good enough
	int ratio = 10000 * dx / -dy;

	// Nb using -ve dy - this means the model below is based on a
	// traditional set of axis, X increasing to right, Y increasing
	// vertically....
	
	if ( ratio < -22998 )  //tan -66.5 = -2.2998
		return (dx > 0) ?
			GestureReader::Point::Right :
			GestureReader::Point::Left ;

	if ( ratio < -4142 )  //tan -22.5 = -0.4142
		return (dx > 0) ?
			GestureReader::Point::DownRight :
			GestureReader::Point::UpLeft ;

	if ( ratio < 0 )      // axis
		return (dx > 0) ?
			GestureReader::Point::Down :
			GestureReader::Point::Up ;

	if ( ratio < 4142 )   //tan -22.5 = -0.4142
		return (dx > 0) ?
			GestureReader::Point::Up :
			GestureReader::Point::Down ;

	if ( ratio < 22998 )  //tan 66.5 = 2.2998
		return (dx > 0) ?
			GestureReader::Point::UpRight :
			GestureReader::Point::DownLeft ;
	else  
		return (dx > 0) ?
			GestureReader::Point::Right :
			GestureReader::Point::Left ;
}


Gesture GestureReader::matchOne(QList<Gesture> gs)
{
	QString myShape;

//	foreach (Point o, p->points) {
//		DEBUG("\n\nPath " << "x,y  : "<<o.x<<","<<o.y<< "  len2 : "<<o.len2
//			  << "  dir  : "<<StrokeCodes+o.direction * STROKECODELEN
//			  << "  type : "<<TouchCodes+o.type * STROKECODELEN);
//	}

	int n = 0;
	do { // until simplified

		// This generates strokes and condenses the list at the same time.
		myShape = generateStrokes();
		
		foreach (const Gesture& g, gs) {
			// has to be one passed in as they are enriched with
			// connection info
			DEBUG("Compare g.shape(" << g.shape() <<") == '"<< myShape <<"'" );
			if (g.shape() == myShape) {
				return g;
			}
		}

		// Now remove shortest paths
		n = 0;
		{
			QMutableListIterator<Point> *i = new QMutableListIterator<Point>(p->points);
			while (i->hasNext()) {
				if (i->next().type == Point::Move) {
					if (i->value().len2 == p->smallestStrokeLen) {
						i->remove();
						p->smallestStrokeLen = 0;  // reset to 0 so we only remove 1 path
					} else {
						n++;                       // count undeleted paths
					}
				}
			}
			delete i;
		}

//		foreach (Point o, p->points) {
//			DEBUG("\n\nPath " << "x,y  : "<<o.x<<","<<o.y<< "  len2 : "<<o.len2
//				  << "  dir  : "<<StrokeCodes+o.direction * STROKECODELEN
//				  << "  type : "<<TouchCodes+o.type * STROKECODELEN);
//		}

	} while (n >= 1); // simplify down to 1 stroke
	
	return Gesture("");
}

QString GestureReader::generateStrokes()
{
	_ENTER;
	QString strokes;
	strokes = "";
	QList<Point> condensed;
	Point oo;
	p->smallestStrokeLen = 999999999;
	{ // this block ensures the iterator goes out of scope and destructs before we delete the list
		QListIterator<Point> o(p->points);
		while (o.hasNext()) {
			oo = o.next();
			if (oo.type != Point::Move) {
				condensed.append(oo);
				// write out the touch code
// Don't do multi-stroke .... yet...				
//			strokes += TouchCodes+o.value().type * STROKECODELEN;
			} else { // a Move
//				DEBUG("Condensing ... len:" << oo.len2);
				while (o.hasNext()
					   && o.peekNext().type == Point::Move
					   && o.peekNext().direction == oo.direction)
				{
					oo.len2 = sqr(oo.len2) + sqr(o.peekNext().len2); // join
					oo.len2 *= oo.len2;
//					DEBUG("removing path len: " << o.peekNext().len2);
					o.next();
				}
//				DEBUG("Condensed ... len:" << oo.len2);
				condensed.append(oo);
				// write out the stroke
				strokes += StrokeCodes+oo.direction * STROKECODELEN;
				// Whilst we're here, keep an eye out for the shortest stroke
				if (oo.len2 && oo.len2 < p->smallestStrokeLen) p->smallestStrokeLen = oo.len2;
			}
		}
	}
	// replace the list
	p->points = condensed;
	DEBUG("Found " << strokes);
	return strokes;
}
