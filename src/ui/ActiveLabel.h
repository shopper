/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef ACTIVELABEL_H
#define ACTIVELABEL_H

#include "shopper.h"           // automake, i8n, gettext
#include <QLabel>
#include <QMouseEvent>

namespace Shopper
{
	// ActiveLabel a label that emits pressed() when clicked
	class ActiveLabel: public QLabel
	{
		Q_OBJECT;			
	public:
		explicit ActiveLabel(QWidget * parent = 0, Qt::WindowFlags f = 0 );
		explicit ActiveLabel(const QString & text, QWidget * parent = 0, Qt::WindowFlags f = 0);
		void mouseReleaseEvent ( QMouseEvent * event );
	signals:
		void pressed();
	};
}

#endif // ACTIVELABEL_H
