/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef SHOPPER_UI_PREFERENCESDIALOG_H
#define SHOPPER_UI_PREFERENCESDIALOG_H
#include "shopperList.h"
#include <QDialog>
#include <QString>
#include <QSignalMapper>
#include "CatListModel.h"

class QCheckBox;
class QGridLayout;
class QTabWidget;
class QDialogButtonBox;
class QWidget;

namespace Shopper
{
// private base for adding/editing an Item
	class PreferencesDialog : public QDialog
	{
		Q_OBJECT;
	public:
		PreferencesDialog(QWidget *parent = 0);

	public slots:
		void toggleSetting(const QString setting);

	private:
		QCheckBox* addPref(const QString setting, bool defaultVal, QString label, QGridLayout *layout, int row);
			
		// GUI
		QTabWidget        *tabWidget;
		QDialogButtonBox  *buttonBox;
		QWidget           *uiPane;   // Show hide elements, columns etc
		QWidget           *dataPane; // Save on exit?
		QSignalMapper     signalMapper;
	};
}
#endif
