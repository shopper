/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef SHOPPER_UI_NOTIFY_H
#define SHOPPER_UI_NOTIFY_H

#include <QSystemTrayIcon>

namespace Shopper
{
	class Notify : public QSystemTrayIcon
	{
		Q_OBJECT;
	public:
		Notify(const QIcon & icon);
		
		void showMessage ( const QString & title,
								   const QString & message,
								   MessageIcon icon = Information,
								   int millisecondsTimeoutHint = 10000);
	};
}
extern Shopper::Notify *notify;
#endif
