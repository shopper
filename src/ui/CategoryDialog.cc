/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER
#include "CategoryDialog.h"
#include "shopper.h"           // automake, i8n, gettext

#include "shopperList.h"
#include "LabelEntry.h"

#include <QDesktopWidget>
#include <QToolButton>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QScrollArea>
#include <QObject>
#include <QStyle>
#include <QString>
#include <QLabel>
#include <QApplication>

namespace Shopper
{
	struct CategoryDialog::Private {
		// Data
		Shopper::List      *mylist;
		Shopper::Category  *mycat;

		// UI
		QFingerScrollArea  *areaS;
		QVBoxLayout        *areaL;
		QWidget            *area;

	};
////////////////////////////////////////////////////////////////
// Public
	CategoryManage::CategoryManage(QWidget * parent, Shopper::List* l) :
		CategoryDialog(parent, l, 0)
	{}

////////////////////////////////////////////////////////////////
// Private  class CategoryDialog : public QDialog
	CategoryDialog::CategoryDialog(QWidget * parent, Shopper::List* l, Shopper::Category* cat) :
		QDialog(parent)
	{
		_ENTER;
		p= new Private;
		p->mylist = l;
		p->mycat = cat;
		p->area = 0;
		// Make the dialog tall and not too wide.
		resize(qApp->desktop()->screenGeometry().width()*70/100,
			   qApp->desktop()->screenGeometry().height()*90/100);

		// Buttons
		QDialogButtonBox* bbox = new QDialogButtonBox(Qt::Horizontal, this);	
		bbox->addButton(tr("Done"), QDialogButtonBox::AcceptRole);
		connect(bbox, SIGNAL(accepted()), this, SLOT(accept()));
		
		connect(bbox->addButton(tr("Add Category"), QDialogButtonBox::ActionRole),
				SIGNAL(clicked()),
				this, SLOT(add_category()));

		// Scroll area with category views
		p->areaS = new QFingerScrollArea(this);
		p->areaS->setWidgetResizable(true);

		// moved from update
		p->area = new QWidget;
		p->areaL = new QVBoxLayout(p->area);
		p->area->setLayout(p->areaL); // set the layout and take parentage of all children
		p->areaS->setWidget(p->area); // add area to the scroller

		// Build them up
		QVBoxLayout* vbox = new QVBoxLayout;
		vbox->addWidget(p->areaS);
		vbox->addWidget(bbox);
		setLayout(vbox);
		show();		
		// add the categories to the area.
		update_area();

		// Now connect the List's  category_list_changed() signal to this object's updater
		connect (l, SIGNAL(category_list_changed()),
				 this, SLOT(update_area()));

		// When a category is added make sure the scroll area is in the right place
		// and the CatView is focused. This needs to be a queued connection.
		connect (this, SIGNAL(categoryAdded(CatView *)),
				 this, SLOT(highlightCategory(CatView *)), Qt::QueuedConnection);

	}
	CategoryDialog::~CategoryDialog()
	{
		delete p;
	}
	
	void CategoryDialog::update_area()
	{
		_ENTER;
		// Clean up our old widgets
		QObject *child;
		QWidget *wChild;
		foreach (child, p->area->children()){
			if (child->isWidgetType()) {
				wChild = static_cast<QWidget*>(child);
				DEBUG("Kill a child " << child->metaObject()->className()  );
				p->areaL->removeWidget(wChild);
				wChild->deleteLater();
			}
		}

		// lots of these
		CatView *cv;
		int pos;
		Shopper::List::pCategoryIter begin =p->mylist->categoriesI(),
			end = p->mylist->categoriesEnd();
		Shopper::List::pCategoryIter catI;
		for(catI = begin, pos=0; catI != end; ++catI,++pos)
		{
			cv = new CatView(**catI, *(p->mylist), p->area);
			p->areaL->addWidget(cv);   // add CatView to the layout
			connect(cv, SIGNAL(move_up()),
					this, SLOT(move_up()));
			connect(cv, SIGNAL(move_down()),
					this, SLOT(move_down()));
			if (catI == begin) cv->set_pos(pos,-1); // size of -1 won't be reached
		}
		cv->set_pos(pos-1,pos-1); // Now we know the end
	}
	
	void CategoryDialog::add_category()
	{
		_ENTER;
		// Create a new category
		p->mycat = new Shopper::Category("New Category");
		p->mylist->add(*p->mycat);
		p->mylist->make_category_active(*p->mycat);

		// Give the scrollbar chance to update with all the new widgets
		qApp->processEvents();
		
		// Adding the category should update the area to include the new cv...
		// so find it and emit a change which will be scheduled to occur from
		// the event loop.
		QObject *child;
		foreach (child, p->area->children()){
			if (child->isWidgetType()) {
				CatView *cv;
 				cv = dynamic_cast<CatView *>(child);
				if (cv && cv->get_cat() == p->mycat) {
					emit categoryAdded(cv);
					return;
				}
			}
		}
	}

	void CategoryDialog::highlightCategory(CatView *cv)
	{
		cv->activate();
		p->areaS->ensureWidgetVisible(cv);
	}
	
#define DOWN_SCREEN 1
#define UP_SCREEN -1
	void CategoryDialog::move_up()
	{
		_ENTER;
		CatView *src = static_cast<CatView*>(this->sender());
		move(src, UP_SCREEN);
	}
	void CategoryDialog::move_down()
	{
		_ENTER;
		CatView *src = static_cast<CatView*>(this->sender());
		move(src, DOWN_SCREEN);
	}
	void CategoryDialog::move(CatView* src, int direction)
	{
		_ENTER;
		int pos = p->areaL->indexOf(src);
		int max = p->areaL->count()-1;
		DEBUG("src at pos=" << pos << "/"<<max<<" direction=" << direction);
		
		if (direction == DOWN_SCREEN and pos == max) return;	// moving 'down' the screen
		if (direction == UP_SCREEN   and pos == 0)  return;
		
		QLayoutItem *li = p->areaL->itemAt(pos+direction);
		if (li == 0) {
			DEBUG("No item found at pos=" << pos << " direction=" << direction);
			return;
		}
		CatView *that = static_cast<CatView*>(p->areaL->itemAt(pos+direction)->widget());
		Q_ASSERT(that != 0);

		p->areaL->removeWidget(src);
		p->areaL->insertWidget(pos+direction, src);
		p->mylist->swap_categories(src->get_cat(), that->get_cat());
		src->set_pos(pos+direction,max);
		that->set_pos(pos,max);
		p->area->show();
	}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

// The widget shows the state of a category
// it can move up or down and edits the relevant category.
	struct CatView::Private {
		LabelEntry *name;		
		// UI
		QToolButton *up_b;
		QToolButton *down_b;
		
		// member data
		Category       *mycat;
		List           *mylist;
	};

	CatView::CatView(Category &cat, List &lst, QWidget *parent) :
		QWidget(parent)
	{
		_ENTER;
		p= new Private;
		p->mycat = &cat;
		p->mylist = &lst;
		// UI
		p->name = new LabelEntry(cat.get_name(), this);
		p->up_b = new QToolButton(this);
		p->up_b->setArrowType(Qt::UpArrow);
		p->down_b = new QToolButton(this);
		p->down_b->setArrowType(Qt::DownArrow);
		QToolButton *del_b = new QToolButton(this);
		del_b->setIcon(style()->standardIcon(QStyle::SP_TrashIcon));
		QHBoxLayout *box = new QHBoxLayout(this);

		box->addWidget(del_b);
		box->addWidget(p->name,1);
		box->addWidget(p->up_b);
		box->addWidget(p->down_b);
		setLayout(box);
		
		// Respond to changing the label
		connect(p->name, SIGNAL(changed()),
				this, SLOT(name_changed()));
		// Respond to del
		connect(del_b, SIGNAL(clicked()),
				this, SLOT(delete_category()));
		// Respond to up
		connect(p->up_b, SIGNAL(clicked()),
				this, SIGNAL(move_up()));
		// Respond to down
		connect(p->down_b, SIGNAL(clicked()),
				this, SIGNAL(move_down()));
		
		// Make the container visible according to visibility logic
		DEBUG("CatView created for " <<cat.get_name());
	}

	CatView::~CatView()
	{
		delete p;
	}
	
	void CatView::delete_category()
	{
		DEBUG("Delete Category");
		
		// Message
		QString l;
		int s  = p->mycat->get_size();
		if (s > 0) {
			l =  "Category '" + p->mycat->get_name()
				+ "' has " + QString::number(s)
				+ " items. Delete them all? ";
		} else {
			l = "Delete empty category '" + p->mycat->get_name() + "'?";
		}
		
		QDialog dia(this);
		dia.setWindowTitle("Are you sure?");
		
		QVBoxLayout vbox(&dia);
		QDialogButtonBox bbox(QDialogButtonBox::Yes | QDialogButtonBox::No, Qt::Horizontal, &dia);
		connect(&bbox, SIGNAL(accepted()), &dia, SLOT(accept()));
		connect(&bbox, SIGNAL(rejected()), &dia, SLOT(reject()));
		vbox.addWidget(new QLabel(l, &dia));
		vbox.addWidget(&bbox);
		dia.setLayout(&vbox);

		if (dia.exec() == QDialog::Accepted)
		{
//FIX			Hildon::Banner::show_information(*this, "Deleted category " + p->mycat->get_name());
			p->mylist->rm(*p->mycat);
		}
	}

	void CatView::name_changed()
	{
		p->mycat->set_name(p->name->getText()); 
	}
	void CatView::set_pos(int pos, int size)
	{
		_ENTER;
		if (pos == 0) p->up_b->hide(); else p->up_b->show();
		if (pos == size) p->down_b->hide(); else p->down_b->show();
		show();
	}

	void CatView::activate()
	{
		_ENTER;
		p->name->label_clicked();
	}
	
	Category * CatView::get_cat()
	{
		return p->mycat;
	}
}
