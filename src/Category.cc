/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1

#include "shopper.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <list>
#include <set>
#include <algorithm>
#include <signal.h>
#include "shopperList.h"

using namespace std;

namespace Shopper
{
////////////////////////////////////////////////////////////////
//  Category
	int Category::id_master = 0;
	Category::Category(sparseCategory dummy) : // SPARSE constructor for ListParser
		id(0),
		name(),
		_wanted(0),
		_bought(0)
	{ Q_UNUSED(dummy) }
	Category::Category() :
		id (Category::id_master++),
		_wanted(0),
		_bought(0)
	{
	}
	Category::Category(QString name) :
		id (Category::id_master++),
		name(name),
		_wanted(0),
		_bought(0)
	{}
	Category::~Category()
	{
		Item* it;
		while (!items.empty())  // Can't use an iterator as rm() modifies the list... <sigh>
		{
			it = items.back();
			rm(*it);
		}
		emit deleted();
	}

	void Category::dbg() {
		DEBUG("\nCat  name : " << name <<
			  "\n       id : " << id <<
			  "\n       wanted : " << _wanted <<
			  "\n       bought : " << _bought <<
			  "\n");
	}

    // Operators
	bool operator< (const Category &a, const Category &b){
		return (a.id < b.id);
	}
	bool cat_cmp (const Category *a, const Category *b)	{
		return (a->id < b->id);
	}
    // cmp (user defined)
//	bool user_cmp (const Category &a, const Category &b)
//	{
//		return (a.pos < b.pos);
//	}

	// accessors
	int Category::get_id() const  {	return id; }
	void Category::set_id(int i) {
		if (id != i) {
			id = i;
			emit changed();
		}
	}
	void Category::set_name(QString n) {
		if (name != n) {
			name = n;
			emit changed();
		}
	}
	QString Category::get_name() const  { return name; }
	int Category::get_size() const {	return items.size(); }

	void Category::setWanted(bool b) { b?_wanted++:_wanted--; emit changed(); }
	void Category::setBought(bool b) { b?_bought++:_bought--; emit changed(); }
	int Category::wanted() const { return _wanted; }
	int Category::bought() const { return _bought; }

	
	void Category::sort()
	{
		items.sort(cmpItem);
		emit changed();
	}
	void Category::add(Item &it)
	{
		DEBUG("Category add item " << it.get_desc() << "\n");
		items.push_back(&it);
		items.sort(cmpItem);
		emit item_added(&it);
	}
	void Category::rm(Item &it, bool deleteItem)
	{
		DEBUG("Category remove item " << it.get_desc() << "\n");
		items.remove(&it);
		emit item_removed(&it);
		if (deleteItem) delete &it;
	}
	
	Category::pItemIter Category::itemsI() {return items.begin();}
	Category::pItemIter Category::itemsEnd(){return items.end();}
	int Category::size() const {return items.size();}

}
