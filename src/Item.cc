/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
//#define DEBUG_SHOPPER 1

#include "shopper.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <list>
#include <set>
#include <algorithm>
#include <signal.h>
#include "shopperList.h"

using namespace std;

namespace Shopper
{
	int Item::id_master = 0;
	
	Item::Item(sparseItem dummy) : // SPARSE constructor for ListParser
		id (Item::id_master++),
		category (0),
		wanted (false),
		bought (false)		
	{ Q_UNUSED(dummy) }
	Item::Item() :
		id (Item::id_master++),
		category (0),
		wanted (false),
		bought (false)		
	{}
	Item::Item(Category &c, QString d, QString n, bool w, bool b) :
		id (Item::id_master++),
		category (&c),
		desc (d),
		note (n),
		wanted (false),
		bought (false)
	{
		set_wanted (w);
		set_bought (b);

		DEBUG("Created an item from parts\n");
	}
	
	Item::~Item()
	{
		emit deleted();
	}

	void Item::dbg() {
		DEBUG(
			"\nItem desc : " << desc <<
			"\n       id : " << id <<
			"\n     note : " << note <<
			"\n   bought : " << (bought ? "true":"false") <<
			"\n   wanted : " << (wanted ? "true":"false") <<
			"\n category : " << category->get_name() <<
			"\n");
	}
	// accessors
	int Item::get_id() const { return id; }
	void Item::set_id(int i){ id = i; }
	void Item::set_desc(QString d){
		if (desc != d) {
			desc = d;		
			emit changed();
			category->sort();
		}
	}
	QString Item::get_desc() const { return desc; }

	void Item::set_note(QString n) {
		if (note != n) {
			note = n;
			emit changed();
		}
	}
	QString Item::get_note() const { return note; }
	void Item::set_wanted(bool w) {
		if (wanted !=w) {
			DEBUG("Wanted\n");
			wanted =w;
			category->setWanted(wanted);
			emit changed();
		}
	}
	bool Item::get_wanted() const { return wanted; }
	void Item::set_bought(bool b)
	{
		if (bought != b) {
			DEBUG("Bought\n");
			bought = b;
			category->setBought(bought);
			emit changed();
		}
	}
	bool Item::get_bought() const { return bought; }
	void Item::set_category(Category *c){
		if (category != c) {
			category->rm(*this, false);
			category = c;
			category->add(*this);
			emit changed();
		}
	}
	Category* Item::get_category() { return category; }
	
	bool operator< (const Item &a, const Item &b){
		return (a.desc < b.desc);
	}
	bool cmpItem(const Item *a, const Item *b){
		return (a->desc < b->desc);
	}
}
