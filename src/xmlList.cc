/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

//#define DEBUG_SHOPPER 1
#include "shopper.h"           // automake, i8n, gettext
#include <QXmlAttributes>
#include <QXmlParseException>
#include <QXmlSimpleReader>
#include <QXmlInputSource>
#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <list>
#include <set>
#include <signal.h>
#include <cstdlib>
#include "shopperList.h"


using namespace std;

namespace Shopper
{
	void abort (QString msg){
#ifndef DEBUG_SHOPPER
		Q_UNUSED(msg)
#endif
		DEBUG(msg);
		exit(1); // FIXME
	}

	ListParser::ListParser(List *list):
		l(list),
		active_cat_id(0)
	{
	}
	
	void ListParser::start_list_el(const QXmlAttributes & attrs)
	{
		// zero the id counters
		Category::id_master = 0;
		Item::id_master = 0;
		QString tmp("name");
		for (int at = 0; at != attrs.count(); at++) {
			QString at_name = attrs.localName(at);
			QString at_value = attrs.value(at);
			if (at_name.toLower() == "name") {
				l->name = at_value;
			} else if (at_name.toLower() == "state") {   // save as int? or human readable?
				bool ok;
				l->state = sState(at_value.toInt(&ok));
				if (!ok) {
					abort("Converting state failed\n");
				}
			} else {
				DEBUG("<list> Unknown attribute : " << at_name << "='" <<at_value<<"'\n");
			}
		}
	}
	void ListParser::start_cat_el(const QXmlAttributes & attrs)
	{
		if (l == 0) {
			DEBUG("<category> Not inside <list>\n");
			exit(1); // FIXME throw?
		}
		c = new Category();
		for (int at = 0; at != attrs.count(); at++) {
			QString at_name = attrs.localName(at);
			QString at_value = attrs.value(at);
			if (at_name.toLower() == "name") {
				c->name = at_value;
			} else if (at_name.toLower() == "active") { // is this the category active?
				if (at_value.toLower() == "true" or at_value == "1")
					l->active_category = c;
			} else {
				DEBUG("<category> Unknown attribute : " << at_name << "='" <<at_value<<"'\n");
			}
		}
		l->add(*c);
	}
	void ListParser::start_item_el(const QXmlAttributes & attrs)
	{
		if (c == 0) {
			DEBUG("<item> Not inside <category>\n");
			exit(1); // FIXME
		}

		i = new Item();
		i->category=c; // current category
		for (int at = 0; at != attrs.count(); at++) {
			QString at_name = attrs.localName(at);
			QString at_value = attrs.value(at);
			if (at_name.toLower() == "desc") {
				i->desc = at_value;
			} else if (at_name.toLower() == "note") {
				i->note = at_value;
			} else if (at_name.toLower() == "wanted") {
				i->set_wanted(at_value.toLower() == "true" or at_value == "1");
			} else if (at_name.toLower() == "bought") {
				i->set_bought(at_value.toLower() == "true" or at_value == "1");
			} else {
				DEBUG("<item> Unknown attribute : " << at_name << "='" <<at_value<<"'\n");
			}
		}
		l->add(*i);
	}

	bool ListParser::startElement (const QString & namespaceURI,
								   const QString & el,
								   const QString & qName,
								   const QXmlAttributes & attrs)
	{
		Q_UNUSED(namespaceURI)
		Q_UNUSED(qName)
		DEBUG("adding " << el << "\n");
		if (el.toLower() == "list") {
			start_list_el(attrs);
		} else if (el.toLower() == "category") {
			start_cat_el(attrs);
		} else if (el.toLower() == "item") {
			start_item_el(attrs);
		} else {
			throw ;
		}
		return true;
	}

	bool ListParser::endElement (const QString & namespaceURI,
								 const QString & el,
								 const QString & qName)
	{
		Q_UNUSED(namespaceURI)
		Q_UNUSED(qName)
		DEBUG("done " << el << "\n");
		if (el.toLower() == "list") {
			l->resequence(); // Ensure the id's are sensible FIXME : not needed?
			l = 0; // No current list
		} else if (el.toLower() == "category") {
			// add the created cat to list
			c->dbg();
			c->items.sort(cmpItem);
			c=0; // No current category
		} else if (el.toLower() == "item") {
			// add the created item to list
			i->dbg();
			DEBUG("Assigned " << i->desc << " to category " << i->category->name << "\n");
			i = 0; // No current item
		} else {
			return false;
		}
		return true;
	}
	bool ListParser::fatalError ( const QXmlParseException & exception )
	{
		Q_UNUSED(exception)
		DEBUG("Markup error\n");
		return true;
	}

	void ListParser::from_string(QString xml)
	{
		QXmlSimpleReader xmlReader;
		xmlReader.setContentHandler(this);
		xmlReader.setErrorHandler(this);
		QXmlInputSource source;
		source.setData(xml);
		bool ok = xmlReader.parse(&source);
		if (!ok)
			std::cout << "Parsing failed." << std::endl;
		DEBUG("Parsing EXIT\n");
	}

////////////////////////////////////////////////////////////////
	XMLWriter::XMLWriter(const Shopper::List *list) :
		l(list)
	{}
		

	ostream& XMLWriter::write(ostream &out)
	{
		DEBUG("in write\n");
		out << "<list name='"<< qPrintable(l->name)
			<< "' state='" << l->state
			<< "'>\n";
		for (List::pCategoryIter c = l->categories.begin(); c != l->categories.end(); c++) {
			out << "  <category"
				<< ((l->active_category == *c) ? " active='1' " : "")
				<< " name='" << qPrintable((*c)->name)
				<< "' id='" << QString::number((*c)->id)
				<< "'>\n";
			for (Category::pItemIter i = (*c)->items.begin(); i != (*c)->items.end(); i++) {
				out << "  <item"
					<< " wanted='" << (*i)->wanted
					<< "' bought='" << (*i)->bought
					<< "' desc='" << qPrintable((*i)->desc) // FIXME: Escape quotes etc
					<< "' note='" << qPrintable((*i)->note) // FIXME: Escape quotes etc
					<< "'/>\n";
			}
			out << "  </category>\n";
		}
		out << "</list>\n";
		DEBUG("done write\n");
		return (out);
	}
}
