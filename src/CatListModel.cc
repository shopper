/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

//#define DEBUG_SHOPPER 1
#include "CatListModel.h"
#include "shopper.h"           // automake, i8n, gettext

namespace Shopper
{   // class CatListModel : public QAbstractTableModel
	CatListModel::CatListModel(Shopper::List &l, bool show_everything1) :
		mylist(&l),
		show_everything(show_everything1)
	{
		_ENTER;
		connect(mylist, SIGNAL(category_list_changed()),
				this, SLOT(listChanged()));
		connect(mylist, SIGNAL(active_category_changed()),
				this, SLOT(activeCategoryChanged()));
	}

	////////////////////////////////////////////////////////////////
	// Interface Methods
	int CatListModel::currentIndex(){
		return indexOfCurrentCat();
	}
	////////////////////////////////////////////////////////////////
	// Interface Slots
	void CatListModel::listChanged ()
	{
		emit layoutAboutToBeChanged();
		emit layoutChanged();
	}
	void CatListModel::activeCategoryChanged()
	{
		int i= indexOfCurrentCat();
		DEBUG("emit currentChanged(" <<i <<")");
		emit currentChanged(i);
	}

	////////////////////////////////////////////////////////////////
	// Utility
	int CatListModel::indexOfCurrentCat()
	{
		Shopper::Category *ac = mylist->get_active_category();
		if (ac == 0) return 0;
		return indexOfCat(ac);
	}

	int CatListModel::indexOfCat(Shopper::Category *cat)
	{
		int i = (show_everything?1:0);
		Shopper::List::pCategoryIter c;
		Shopper::List::pCategoryIter c_end;
		for (c = mylist->categoriesI(); *c!=cat and c!=c_end; i++, c++)
			;
		Q_ASSERT(c!=c_end);
		return i;
	}

	////////////////////////////////////////////////////////////////
	// Override virtuals
	int CatListModel::rowCount(const QModelIndex & parent) const
	{
		Q_UNUSED(parent)
		DEBUG("rowCount is " << mylist->categories.size()+(show_everything?1:0));
		return mylist->categories.size()+(show_everything?1:0);
	}
	int CatListModel::columnCount(const QModelIndex & parent) const
	{
		Q_UNUSED(parent)
		return 2;
	}
	QVariant CatListModel::headerData ( int section, Qt::Orientation orientation, int role ) const
	{
		Q_UNUSED(orientation)
		Q_UNUSED(role)
		DEBUG("Role " << role);
		switch (section) {
		case 0:	 return QVariant("Ptr");
		case 1:	 return QVariant("Data");
		default: return QVariant();
		}				
	}
	QVariant CatListModel::data ( const QModelIndex & index, int role ) const
	{
//		_ENTER;
		if (role != Qt::UserRole and
			role != Qt::DisplayRole and
			role != Qt::DecorationRole
			) {  // FIX: This should be done properly
//			DEBUG("Unsupported Role " << role);
			return QVariant();
		}
//		int col = index.column();
		int i = index.row() + (show_everything?0:1); // This works.
//		DEBUG("Asked for (translated) " << i <<":"<<col << "  Role:" << role <<  (show_everything?"  show:yes":"  show:no"));
		if (i == 0) { // 0 = Everything
			if (role == Qt::UserRole) {
				return QVariant();
			} else {
//				DEBUG("Giving string Everything");
				return QVariant(tr("Everything"));
			}
		}
		Shopper::Category *cat;
		Shopper::List::pCategoryIter c;
		Shopper::List::pCategoryIter end=mylist->categoriesEnd();
		// Ugh... ??? unless we reimpliment Shopper::List

		c = mylist->categoriesI();
		do {
			Q_ASSERT(c != end);
			Q_CHECK_PTR(*c);
		} while (--i and ++c!=end);
	   
		cat = *c;
		Q_CHECK_PTR(cat);
		QString name = cat->get_name();
		
		Q_CHECK_PTR(cat);
		if (role == Qt::UserRole) {
//			DEBUG("Giving pointer to " << cat->get_name());
			return QVariant::fromValue(cat);
		} else {
//			DEBUG("Giving string " << cat->get_name());
			return QVariant(cat->get_name());
		}
	}

     // Use like this:
//	 Category *c;
//	 if (v.canConvert<Category*>())
//	 c = v.value<Category*>();
	
}

