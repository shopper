/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

//#define DEBUG_SHOPPER 1

#include "shopper.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <list>
#include <set>
#include <algorithm>
#include <signal.h>
#include "shopperList.h"

using namespace std;

namespace Shopper
{
	List::List() :
		name("Sainsburys"),
		state(MAKING_LIST),
		active_category(0)
	{
		// No items, no categories
		DEBUG("\n\n\nMAKING NEW LIST\n\n\n");
	}
	List::List(QString dump)
	{
		DEBUG("\n\n\nMAKING NEW LIST FROM STRING\n\n\n");
		ListParser parse(this);
		parse.from_string(dump);
	}   // initiate from a dump

	List::~List()
	{
		for(list<Category*>::iterator catI = categories.begin();
			catI != categories.end(); ++catI) {
			delete *catI;
		}
	}

	List* List::from_file(QString filename)
	{
		// open file
		std::ifstream file(filename.toAscii());
		if (!file) {
			return 0;
		}
		// Read xml as a stream
		std::stringstream xml;
		xml << file.rdbuf();
		std::string sxml(xml.str());
		QString qxml(sxml.c_str());
		DEBUG("read from : " <<filename <<"\n"<<qxml);
		// Create new list and return it
		return new Shopper::List(qxml); // FIXME: may not succeed.
	}

	void List::to_file(QString filename)
	{
		std::ofstream outf(filename.toAscii());
		if (!outf) {
			DEBUG("Couldn't open " << filename << " for writing\n");
			exit(1);
		}
		outf << *this ;
	}

	List::operator QString() {return QString("XML here");}    // a ustring representation of a List (XML)

    // an ostream representation of a List (XML)
	std::ostream& operator <<(std::ostream &out, const List &l)
	{
		DEBUG("in << operator\n");
		XMLWriter w(&l);
		DEBUG("written XML\n");
		return w.write(out);
	}

	// These are now just helper functions
	void List::add(Item &it)
	{
		it.get_category()->add(it);
	}
	void List::rm(Item &it)	   
	{
		it.get_category()->rm(it);
	}

	void List::add(Category &cat)
	{
		DEBUG("Add category " << cat.get_name());
		if (categories.empty())
		{
			DEBUG(" made active\n");
			categories.push_back(&cat);
			make_category_active(cat);
		} else {
			list<Category*>::iterator low = lower_bound(categories.begin(),categories.end(), &cat, &cat_cmp);
			categories.insert(low, &cat);
			resequence();
		}
		// Allows the list to emit signals when Items are added/removed
		connect (&cat, SIGNAL(item_added(Item*)),
				 this, SIGNAL(item_added(Item*)));
		connect (&cat, SIGNAL(item_removed(Item*)),
				 this, SIGNAL(item_removed(Item*)));
		emit category_list_changed();
		emit category_added(&cat);
	}
	void List::rm(Category &cat)
	{
		if (categories.size() == 1) return;
		DEBUG("Removing category\n");
		if (is_category_active(cat)) cycle_active_category(true);
		categories.remove(&cat);
		resequence();
		emit category_removed(&cat);
		emit category_list_changed();
		delete &cat;
	}

	// accessors
	QString List::get_name() const {return name;}
	void List::set_name(QString)
	{
//		emit changed();
	}
	sState List::get_state() const {
		return state;
	}
	void List::set_state(sState s)
	{
		if (state == s) return;

		sState old = state;
		state = s;  // Set the state
		emit state_changed();
		
		if (s == Shopper::MAKING_LIST) { // And go to category '1'
			if (active_category == 0) { // show category 1 rather than 'Everything'
				make_category_active(*(categories.front()));
			}
		}
		// And go to category 'Everything' if switching to an 'out shopping' state.
		if (s == Shopper::WHATS_LEFT || s == Shopper::OUT_SHOPPING) {
			if (old == Shopper::MAKING_LIST) no_category_active();
		}
	}

	// Category filtering
	Category* List::get_active_category()
	{
		return(active_category);
	}
	bool List::is_category_active(Category &c) const  // empty set = all active
	{
		return (active_category == 0 || active_category == &c);
// If using a set for multiple active categories
//		return (active_categories.empty() || ( active_categories.find(c) != active_categories.end() ));;
	}
	void List::cycle_active_category(bool f)
	{
		List::pCategoryIter  begin  = categories.begin();
		List::pCategoryIter  end    = categories.end();
		if (active_category == 0) { // use first or last element as 'next'
			active_category = f ? *(begin) : *(categories.rbegin());
		} else {
			List::pCategoryIter c =
				find(begin, end, active_category);
			if (c == end) {
				DEBUG("Not found - shouldn't happen\n");
				exit(1);
			}
			if (f) {
				active_category = (++c == end)?0:*c;
					} else {
				active_category = (c == begin)?0:*--c;
			}
		}
	}

	void List::make_next_category_active()
	{
		cycle_active_category(true);
		emit active_category_changed();
	}
	void List::make_prev_category_active()
	{
		cycle_active_category(false);
		emit active_category_changed();
	}
	void List::make_category_active(Category &c)
	{
		if (active_category != &c) {
			active_category = &c;
// If using a set for multiple active categories
//		active_categories.insert(c);
			emit active_category_changed();
		}
	}
	void List::make_category_inactive(Category &c)
	{
		// Comment out if you want to use the commented out code below
		Q_UNUSED(c)
		if (active_category != 0) {
			active_category = 0;
// If using a set for multiple active categories
//		active_categories.erase(c);
			emit active_category_changed();
		}
	}
	void List::no_category_active()
	{
		if (active_category != 0) {
			active_category = 0;
			emit active_category_changed();
		}
	}

	void List::resequence()
	{
		int id = 0;
		for(list<Category*>::iterator catI = categories.begin();
			catI != categories.end(); ++catI) {
			(*catI)->set_id(id++);
		}
	}
	void List::reorder()
	{
		categories.sort(&cat_cmp);
	}

	void List::swap_categories(Category *a, Category *b)
	{
		int a_id = a->get_id();
		DEBUG("a id  is : " << a->get_id()
			 << " b id  is : " << b->get_id()
			  << "\n");
		a->set_id(b->get_id());
		DEBUG("Now set b\n");
		b->set_id(a_id);
		DEBUG("a id  is : " << a->get_id()
			 << " b id  is : " << b->get_id()
			  << "\n");
		reorder();
		emit category_list_reordered();
		DEBUG("Reorder done\n");
	}
	void List::swap_categories(int a, int b)
	{
		Q_UNUSED(a)
		Q_UNUSED(b)
		/*Category *c1 = cat_by_id[a];
		Category *c2 = cat_by_id[b];
		c1->set_id(b);
		c2->set_id(a);
		cat_by_id[a] = c2;
		cat_by_id[b] = c1;
		reorder();*/
	}
	
	QString List::modeText()
	{
		return modeText(state);
	}
	QString List::modeText(sState s)
	{
		switch (s) {
		case WHATS_LEFT :
			return(QString("What's Left"));
		case OUT_SHOPPING:
			return(QString("Full List"));
		case MAKING_LIST:
			return(QString("Making List"));
		default:
			return(QString("BAD State"));
		}
	}
	List::pCategoryIter List::categoriesI(){ return categories.begin();	}
	List::pCategoryIter List::categoriesEnd(){ return categories.end(); }

	void List::boughtNothing(){}  // marks all items as not bought
	void List::newList(){}        // marks all items as not wanted

	void List::clear(clear_list_mode m)
	{
		for (List::pCategoryIter c = categories.begin(); c != categories.end(); c++) {
			DEBUG("Clearing : "<<(*c)->get_name());
			for (Category::pItemIter it = (*c)->items.begin(); it != (*c)->items.end(); it++) {
				(*it)->set_bought(false);  // Always clear bought
				if (m == CLEAR_WANTED) {
					(*it)->set_wanted(false);
				}
				DEBUG("Cleared : "<<(*it)->get_desc());
			}
		}
		if (m == CLEAR_WANTED) {
			set_state(Shopper::MAKING_LIST);
		}
	}
}
