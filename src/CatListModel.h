/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef CATLISTMODEL_H
#define CATLISTMODEL_H

#include "shopperList.h"
#include <QAbstractListModel>

namespace Shopper
{
	class CatListModel : public QAbstractTableModel
	{
		Q_OBJECT;
	public:
		explicit CatListModel(Shopper::List &l, bool show_everything = true);
		int rowCount(const QModelIndex & parent = QModelIndex()) const;
		int columnCount(const QModelIndex & parent = QModelIndex()) const;
//		QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole );
		QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
		QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
		int currentIndex();
		int indexOfCat(Shopper::Category *cat);
		static const int ptrIndex = 0;
		static const int nameIndex = 1;
	private:
		Shopper::List *mylist;
		bool show_everything; // If this is true then index 0 is "Everything"
							  // else index 0 is the first category
		int indexOfCurrentCat();
	public slots:
		void listChanged();
		void activeCategoryChanged();
	signals:
		void currentChanged(int);
	};
}

#endif //CATLISTMODEL_H
