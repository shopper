/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef SHOPPER_LIST_H
#define SHOPPER_LIST_H

#include <QObject>
#include <QString>
#include <QXmlDefaultHandler>

#include <iostream>
#include <list>
#include <set>
#include <map>
using namespace std;

class QXmlParseException;
class QXmlAttributes;

namespace Shopper
{
	// some forward declarations...
	class List;
	class Category;
	class Item;

	// types
	typedef enum _clear_list {CLEAR_WANTED,CLEAR_BOUGHT} clear_list_mode;
		
	// This is really application state but it needs saving to XML
	typedef enum sState_ {MAKING_LIST,
						  OUT_SHOPPING,
						  WHATS_LEFT} sState;  
		
	class List : public QObject
	{
		Q_OBJECT;

		// declare some friends used to load/save Lists
		friend class ListParser;
		friend class XMLWriter;
	public:
		// Constructors
		List();
		List(QString dump);   // initiate from a dump
		~List();
		static List* from_file(QString filename);
		void to_file(QString filename);

		void dbg();
		
		//List rawList();  // err dunno
		operator QString();    // ustring cast gives XML
		// ostream (>>) gives XML
		friend std::ostream& operator<< (std::ostream &out, const List &l);
		
		void add(Item &it);         // List will manage Item
		void rm(Item &it);          // Delete item
		void add(Category &cat);    // List will manage Category
		void rm(Category &cat);     // Delete Category and all items
	private:
		// Helper functions used in managing category order
		void resequence();
		void reorder();
		void swap_categories(int, int);
	public:
		void swap_categories(Category *a, Category *b);

	public:
		// accessors
		QString get_name() const;
		void set_name(QString);
		sState get_state() const;
		void set_state(sState);
		void clear(clear_list_mode m);
		
		// Filtering
		Category* get_active_category();
		bool is_category_active(Category &c) const;
		void no_category_active();
		void make_category_active(Category &c);
		void make_category_inactive(Category &c);
		void make_next_category_active();
		void make_prev_category_active();

		// Mode handling
		void boughtNothing();  // marks all items as not bought
		void newList();        // marks all items as not wanted  
		QString modeText();
		static QString modeText(sState s);

		// collection iterators
//		typedef list<Item*>::const_iterator pItemIter;
		typedef list<Category*>::const_iterator pCategoryIter;
//		pItemIter itemsI();
//		pItemIter itemsEnd();
		pCategoryIter categoriesI();
		pCategoryIter categoriesEnd();
				
	signals:
//		void item_list_changed();
		void item_added(Item*);
		void item_removed(Item*);

		void category_list_changed();
		void category_list_reordered();
		void category_added(Category*);
		void category_removed(Category*);

		void changed();
		void state_changed();
		void active_category_changed();
		void deleted();

	private:
		void cycle_active_category(bool f);
		// members:
		QString       name;
		sState              state;           // making, out:all, out:left		
		Category*           active_category ; 
// If using a set for multiple active categories
//		set<Category>      active_categories ; 
		map<int, Category*> cat_by_id;
	public:
		list<Category*>     categories;
	private:
		list<Item*>         items;

	};
////////////////////////////////////////////////////////////////

	class ListParser : public QXmlDefaultHandler
	{
		friend class List; // Given protected constructor, only Lists can make ListParsers
	protected:
		ListParser(List *l);   // Expected to be passed 'this'
//		~ListParser();
		void from_string(QString);
		void start_list_el(const QXmlAttributes & atts);
		void start_cat_el(const QXmlAttributes & atts);
		void start_item_el(const QXmlAttributes & atts);

		// QXmlDefaultHandler Interface
		bool 	startElement (const QString & namespaceURI,
							  const QString & localName,
							  const QString & qName,
							  const QXmlAttributes & atts);
		bool 	endElement (const QString & namespaceURI,
							const QString & localName,
							const QString & qName);
		bool 	fatalError ( const QXmlParseException & exception );

		List*     l;
		Category* c;
		Item*     i;
		map<int, Category*> cat_by_id;
		int       active_cat_id;
	};
////////////////////////////////////////////////////////////////
	class XMLWriter
	{
	private:
		XMLWriter(const List *l);   // Expected to be passed 'this'
		std::ostream& write(std::ostream &o);
		friend class List; // Given a private constructor, only Lists can make XMLWriters
		friend std::ostream& operator<< (std::ostream &out, const List &l);
		const List *l;
	};
	
}

#endif //SHOPPERLIST_H



// Ordering
//
// This needs 2 functions: re-sequence and re-order
//
// re-sequence iterates the list in the current list order and sets
// 'id' incrementally
//
// re-order causes the list to examine all the member id's and
// re-order itself
//
// Each category has a sort position - 'id'.
// On load this is used to insert into the <list>
// post-load the list is re-sequenced
//
// On 'add' the category is inserted according to a probably duplicate id
// Then the list is re-sequenced.
//
// To re-order the list, the id's are swapped and the list is re-ordered.
// 
//

