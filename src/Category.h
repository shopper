/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef SHOPPER_CATEGORY_H
#define SHOPPER_CATEGORY_H

#include <QObject>
#include <QMetaType>
#include <iostream>
#include <list>
#include <set>
#include <map>
using namespace std;

namespace Shopper
{
	// some forward declarations...
	class List;
	class Category;
	class Item;

	// normal functions
	bool operator< (const Category &a, const Category &b);
	bool cat_cmp (const Category *a, const Category *b);

////////////////////////////////////////////////////////////////
	class Category : public QObject
	{
		Q_OBJECT;
		
	private:
		typedef enum sparseCategory_ {SPARSE} sparseCategory; // Only friends know about 
		Category(sparseCategory dummy);   // This constructor is for friends to instantiate objects
	public:
		Category();
		Category(const Category &src);
		Category(QString name);
		~Category();

		void dbg();
		void sort();
		// accessors
		QString print_id();
		int get_id() const;
		void set_id(int);
		void set_name(QString);
		int get_size() const;
		void setWanted(bool b);
		int wanted() const;
		void setBought(bool b);
		int bought() const;
		QString get_name() const;
		friend bool operator< (const Category &a, const Category &b);
		friend bool cat_cmp (const Category *a, const Category *b);
		void add(Item &it);
		void rm(Item &it, bool deleteItem = true);
		typedef list<Item*>::const_iterator pItemIter;
		pItemIter itemsI();
		pItemIter itemsEnd();
		int size() const;

		static const Category     EVERYTHING;   /* Used for filter */
	private:
		int            id;          /* The id of the item */
//		int            pos;         /* Allow user defined sorting */
		QString  name;        /* The item name */
		int _wanted;
		int _bought;
		static int     id_master;   /* Used for uniqueness */

		// Items in list
		list<Item*>         items;

	signals:		
		void changed();
		void deleted();
		void item_added(Item*);
		void item_removed(Item*);

		friend class ListParser;
		friend class XMLWriter;
		friend class List;
	};
}
Q_DECLARE_METATYPE ( Shopper::Category* )

#endif //SHOPPER_CATEGORY_H



// Ordering
//
// This needs 2 functions: re-sequence and re-order
//
// re-sequence iterates the list in the current list order and sets
// 'id' incrementally
//
// re-order causes the list to examine all the member id's and
// re-order itself
//
// Each category has a sort position - 'id'.
// On load this is used to insert into the <list>
// post-load the list is re-sequenced
//
// On 'add' the category is inserted according to a probably duplicate id
// Then the list is re-sequenced.
//
// To re-order the list, the id's are swapped and the list is re-ordered.
// 
//

