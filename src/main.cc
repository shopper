/* Shopper
 * Copyright (C) 2009 David Greaves <david@dgreaves.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

//#define DEBUG_SHOPPER 1
#include "shopper.h"

#include <QApplication>

#include "shopperList.h"       // Data objects
#include "ui/mainwindow.h"     // GUI

int main(int argc, char *argv[])
{
//    Q_INIT_RESOURCE(application);

    QApplication app(argc, argv);

	// We need to get the config entry for the list filename
	// gtk uses:   /apps/maemo/shopper/list_filename
//	Shopper::List* list;

	QString filename = "~/shopping.xml";
    MainWindow mainWin(filename);
    mainWin.show();
    return app.exec();
}
std::basic_ostream<char>& operator<<(std::basic_ostream<char>& os, const 
QString& str) {	
   return os << qPrintable(str);
}

