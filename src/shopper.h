/* Shopper
 * Copyright (C) 2008 David Greaves <david@dgreaves.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
#ifndef SHOPPER_SHOPPER_H
#define SHOPPER_SHOPPER_H

//#include "config.h"

// Default location of shopping lists
#define HOME_DIR "/home/user/"
#define DEFAULT_LIST "ShoppingList.xml"

#include <iostream>
#include <libintl.h>
#define _(String) gettext (String)
#define gettext_noop(String) String
#define N_(String) gettext_noop (String)

#define CHECK_NULL 1
#include <qglobal.h>

// Define a << handler for QString
std::basic_ostream<char>& operator<<(std::basic_ostream<char>& os, const 
									 QString& str);

#define _MSG(tag) do {													\
		std::cerr << tag << " " <<  __PRETTY_FUNCTION__ << "\n";			\
	} while(0)

#define _MESSAGE(tag, ...) do {											\
	  std::cerr << tag << " :" << __FILE__ <<":"<< __LINE__				\
		   << " (" <<  __PRETTY_FUNCTION__ << ") "									\
		   << __VA_ARGS__ << "\n";										\
  } while(0)

#ifdef DEBUG_SHOPPER
#define DEBUG(...) _MESSAGE("DEBUG", __VA_ARGS__)
#define _ENTER _MSG("ENTER")
#define _LEAVE _MSG("LEAVE")
#else
#define DEBUG(...) {}
#define _ENTER {}
#define _LEAVE {}
#endif

#endif
