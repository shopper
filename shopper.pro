TEMPLATE = app
TARGET = shopper
#DESTDIR = /usr/bin
DEPENDPATH += . src src/ui
INCLUDEPATH += . src src/ui
QT += xml
CONFIG += debug
LIBS += -lXrandr

# Install stuff...
target.path = /usr/bin
INSTALLS += target

# Input
HEADERS += src/CatListModel.h \
           src/shopper.h \
           src/shopperList.h \
           src/Item.h \
           src/Category.h \
           src/List.h \
           src/ui/CategoryDialog.h \
           src/ui/ItemDialog.h \
           src/ui/PreferencesDialog.h \
           src/ui/LabelEntry.h \
           src/ui/ActiveLabel.h \
           src/ui/NoteWidget.h \
           src/ui/mainwindow.h \
           src/ui/notify.h \
           src/ui/ItemView.h \
           src/ui/CategoryView.h \
           src/ui/ListView.h \
           src/ui/QFingerScrollArea.h \
           src/ui/Gesture.h \
           src/ui/GestureWatcher.h \
           src/ui/GestureReader.h
SOURCES += src/CatListModel.cc \
           src/main.cc \
           src/Item.cc \
           src/Category.cc \
           src/List.cc \
           src/xmlList.cc \
           src/ui/CategoryDialog.cc \
           src/ui/ItemDialog.cc \
           src/ui/PreferencesDialog.cc \
           src/ui/LabelEntry.cc \
           src/ui/ActiveLabel.cc \
           src/ui/NoteWidget.cc \
           src/ui/mainwindow.cc \
           src/ui/notify.cc \
           src/ui/ItemView.cc \
           src/ui/CategoryView.cc \
           src/ui/ListView.cc \
           src/ui/QFingerScrollArea.cc \
           src/ui/Gesture.cc \
           src/ui/GestureWatcher.cc \
           src/ui/GestureReader.cc
